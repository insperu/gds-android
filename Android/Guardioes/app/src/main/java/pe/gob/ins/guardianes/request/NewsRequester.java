package pe.gob.ins.guardianes.request;

import android.content.Context;

import pe.gob.ins.guardianes.model.News;
import pe.gob.ins.guardianes.request.base.BaseRequester;
import pe.gob.ins.guardianes.request.base.Method;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.request.base.Requester;
import pe.gob.ins.guardianes.request.base.RequesterConfig;
import pe.gob.ins.guardianes.helper.DateFormat;
import pe.gob.ins.guardianes.helper.Logger;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class NewsRequester extends BaseRequester {

    private static final String TAG = NewsRequester.class.getSimpleName();

    private static final String GET = "/get";

    public NewsRequester(final Context context) {
        super(context);
    }

    public void getAll(final RequestListener<List<News>> listener) {

        final String url = RequesterConfig.URL + RequesterConfig.Api.NOTICE + GET;

        listener.onStart();

        new Requester(getContext()).request(Method.GET, url, getAuthHeaderMap(), new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    try {

                        final List<News> newsList = new ArrayList<>();
                        newsList.add(new News());

                        final JSONArray jsonArray = new JSONObject(response.getResult()).getJSONObject("data")
                                .getJSONArray("statuses");

                        final SimpleDateFormat format1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
                        final SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                        final SimpleDateFormat format3 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());

                                format1.setLenient(true);

                        for (int i = 0; i < jsonArray.length(); i++) {

                            final JSONObject json = jsonArray.getJSONObject(i);

                            final News news = new News();

                            news.setTitle(json.get("text").toString());
                            news.setLink("https://twitter.com/INSColombia/status/" + json.get("id_str").toString());
                            news.setLike(json.getString("favorite_count"));

                            final Date date = format1.parse(json.get("created_at").toString());

                            news.setPublicationDate(format2.format(date));

                            final int hour = DateFormat.getTwitterDiff(format3.format(date));

                            news.setClock(hour + "h");

                            newsList.add(news);
                        }

                        listener.onSuccess(newsList);

                    } catch (final Exception e) {
                        Logger.logError(TAG, e.getMessage());

                        listener.onError(e);
                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }
}

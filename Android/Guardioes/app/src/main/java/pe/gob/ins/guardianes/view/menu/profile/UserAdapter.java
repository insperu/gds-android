package pe.gob.ins.guardianes.view.menu.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import pe.gob.ins.guardianes.R;

import pe.gob.ins.guardianes.helper.AvatarHelper;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DateFormat;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.manager.PrefManager;

import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.List;

class UserAdapter extends ArrayAdapter<User> {

    private final UserListener listener;
    private User user;

    UserAdapter(final Context context, final List<User> userList, final UserListener listener) {
        super(context, 0, userList);
        user = new PrefManager(context).get(Constants.Pref.USER, User.class);
        this.listener = listener;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup viewGroup) {
        View view = convertView;

        ViewHolder viewHolder;

        if (view == null) {

            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            view = inflater.inflate(R.layout.user_item, viewGroup, false);

            viewHolder = new ViewHolder();

            viewHolder.textViewName = (TextView) view.findViewById(R.id.text_view_name);
            viewHolder.textViewAge = (TextView) view.findViewById(R.id.text_view_age);
            viewHolder.textViewType = (TextView) view.findViewById(R.id.text_view_type);
            viewHolder.imageViewImage = (CircularImageView) view.findViewById(R.id.image_view_image);

            view.findViewById(R.id.linear_layout).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View itemView) {
                    listener.onEdit(getItem(position));
                }
            });

            view.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) view.getTag();
        }

        final User user = getItem(position);

        int j = DateFormat.getDateDiff(user.getDob());

        viewHolder.textViewName.setText(user.getFirstname());
        viewHolder.textViewAge.setText(j + " " + getContext().getString(R.string.anos));
        viewHolder.textViewType.setText(user.getType());

        new AvatarHelper().loadImage(viewGroup.getContext(), viewHolder.imageViewImage, user);

        return view;
    }

    class ViewHolder {

        TextView textViewName;
        TextView textViewAge;
        TextView textViewType;
        CircularImageView imageViewImage;
        ImageView imageTrash;
    }
}
package pe.gob.ins.guardianes.view;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Calendar;
import java.util.Locale;

import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.FileHandler;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.push.HashReceiver;
import pe.gob.ins.guardianes.push.RegisterService;
import pe.gob.ins.guardianes.request.SurveyRequester;
import pe.gob.ins.guardianes.request.UserRequester;
import pe.gob.ins.guardianes.request.base.AuthRequester;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.base.BaseActivity;
import pe.gob.ins.guardianes.view.welcome.WelcomeActivity;

public class SplashActivity extends BaseActivity implements Runnable {

    private static final long WAIT_TIME = 1000;

    private final Handler handler = new Handler();
    private User user;

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.splash);


        Locale locale = new Locale("ES");
        Locale.setDefault(locale);

        final Resources resource = getResources();

        final Configuration configuration = resource.getConfiguration();

        configuration.locale = locale;

        resource.updateConfiguration(configuration, resource.getDisplayMetrics());

        loadDirectory();
    }

    @Override
    public void onResume() {
        super.onResume();

        handler.postDelayed(this, WAIT_TIME);
    }

    @Override
    protected void onPause() {
        super.onPause();

        handler.removeCallbacks(this);
    }

    @Override
    public void run() {
        loadAuth();
    }

    private void loadAuth() {

        user = new PrefManager(SplashActivity.this).get(Constants.Pref.USER, User.class);
        if (user == null) {
            navigateTo(WelcomeActivity.class);
        } else {
            new AuthRequester(this).loadAuth(new RequestListener<User>() {

                @Override
                public void onStart() {
                }

                @Override
                public void onError(final Exception e) {
                    navigateTo(WelcomeActivity.class);
                }

                @Override
                public void onSuccess(final User user) {
                    checkGcmToken(user);
                }
            });
        }
    }

    private void hasSurvey() {

        new SurveyRequester(this).hasSurvey(Calendar.getInstance(), new RequestListener<Boolean>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onError(final Exception e) {
                navigateTo(HomeActivity.class);
            }

            @Override
            public void onSuccess(final Boolean has) {
                navigateTo(HomeActivity.class);
            }
        });
    }

    private void loadDirectory() {

        getExternalCacheDir();

        new FileHandler().createDirectory(Constants.DIRECTORY_TEMP);
    }

    private void checkGcmToken(User user) {
        String gcmVersion = new PrefManager(SplashActivity.this).get(Constants.Pref.GCM_TOKEN_VERSION, String.class);
        if (gcmVersion == null || !gcmVersion.equals("2")) {
            this.user = user;
            LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(HashReceiver.HASH_RECEIVER));
            startService(new Intent(SplashActivity.this, RegisterService.class));
        } else {
            hasSurvey();
        }
    }

    private HashReceiver receiver = new HashReceiver() {

        public void onHash(final String hash) {

            user.setGcmToken(hash);
            user.getGcmTokens().add(hash);
            new UserRequester(SplashActivity.this).addOrUpdate("/user/update", user, null, new RequestListener<String>() {
                @Override
                public void onStart() {

                }

                @Override
                public void onError(Exception e) {
                    hasSurvey();
                }

                @Override
                public void onSuccess(String type) {
                    new PrefManager(SplashActivity.this).put(Constants.Pref.GCM_TOKEN_VERSION, "2");
                    hasSurvey();
                }
            });
        }
    };
}

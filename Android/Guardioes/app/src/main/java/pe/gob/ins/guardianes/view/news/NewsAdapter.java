package pe.gob.ins.guardianes.view.news;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.model.News;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_NEWS = 1;

    private final NewsListener listener;

    private List<News> newsList = new ArrayList<>();

    NewsAdapter(final NewsListener listener, final List<News> newsList) {
        this.listener = listener;
        this.newsList = newsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int viewType) {

        if (viewType == TYPE_HEADER) {

            final View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.notice_header, viewGroup, false);

            return new ViewHolder(view);
        }

        final View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.notice_item, viewGroup, false);

        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof NewsViewHolder) {

            final NewsViewHolder viewHolder = (NewsViewHolder) holder;

            final News news = newsList.get(position);

            viewHolder.textViewTitle.setText(news.getTitle());
            viewHolder.textViewHour.setText(news.getClock());
            viewHolder.textViewLike.setText(news.getLike());
            viewHolder.textViewDate.setText(news.getPublicationDate());

            viewHolder.textViewTitle.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View view) {

                    listener.onNewsSelect(news);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    @Override
    public int getItemViewType(final int position) {
        return position == 0 ? TYPE_HEADER : TYPE_NEWS;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(final View view) {
            super(view);
        }
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_date)
        TextView textViewDate;

        @BindView(R.id.btn_news)
        TextView textViewTitle;

        @BindView(R.id.text_view_like)
        TextView textViewLike;

        @BindView(R.id.text_view_hour)
        TextView textViewHour;

        NewsViewHolder(final View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }
}

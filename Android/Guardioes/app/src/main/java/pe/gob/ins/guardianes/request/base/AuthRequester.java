package pe.gob.ins.guardianes.request.base;

import android.content.Context;

import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.Utility;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.helper.Logger;
import pe.gob.ins.guardianes.manager.PrefManager;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class AuthRequester extends BaseRequester {

    private static final String TAG = AuthRequester.class.getSimpleName();

    public AuthRequester(final Context context) {
        super(context);
    }

    public void loadAuth(final RequestListener<User> listener) {

        final String url = RequesterConfig.URL + "/user/lookup/";

        listener.onStart();

        new Requester(getContext()).request(Method.GET, url, getAuthHeaderMap(), null, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    if (isSuccess(response)) {

                        try {

                            final JSONObject json = new JSONObject(response.getResult());

                            if (json.getBoolean("error")) {

                                listener.onError(new RequestException());

                            } else {

                                User user = loadUser(json.getJSONObject("data"));

                                if (new PrefManager(getContext()).put(Constants.Pref.USER, user)) {

                                    listener.onSuccess(user);
                                }
                            }

                        } catch (final JSONException e) {
                            Logger.logDebug(TAG, e.getMessage());

                            listener.onError(new RequestException());
                        }

                    } else {

                        listener.onError(new RequestException());
                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }

    private User loadUser(JSONObject json) {
        User user = new User();
        try {
            user.setFirstname(json.getString("firstname"));
            user.setLastname(json.getString("lastname"));
            user.setEmail(json.getString("email"));
            user.setGender(json.getString("gender"));
            user.setId(json.getString("id"));
            user.setRace(json.getString("race"));
            user.setDob(json.getString("dob"));
            user.setUserToken(json.getString("token"));
            try {
                user.setLevel(json.getInt("level"));
            } catch (Exception e) {
                user.setLevel(1);
            }

            if (!json.isNull("xp")) {
                user.setEnergy(json.getInt("xp"));
            }

            if (!json.isNull("answers")) {
                final JSONArray array = json.getJSONArray("answers");

                for (int i = 0; i < array.length(); i++) {

                    user.getPieceMap().put(i, array.getInt(i) == 1);
                }
            }

            if (!json.isNull("gcmTokens")) {
                final JSONArray arrGcmToken = json.getJSONArray("gcmTokens");

                for (int i = 0; i < arrGcmToken.length(); i++) {
                    user.getGcmTokens().add(arrGcmToken.getString(i));
                }
            }

            try {
                user.setImage(json.getInt("picture"));

            } catch (Exception e) {
                Logger.logError("AuthRequester:loadUser", "No picture available!");
            }

            if (json.has("country")) {
                user.setCountry(json.getString("country"));
            }

            if (json.has("state")) {
                user.setState(json.getString("state"));
            }

            if (json.has("city")) {
                user.setCity(json.getString("city"));
            }

            try {
                user.setHashtags(Utility.toList(json.getJSONArray("hashtags")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }
}

package pe.gob.ins.guardianes.helper;

import android.content.Context;
import android.net.ConnectivityManager;

import pe.gob.ins.guardianes.manager.LocationManager;

/**
 * @author Igor Morais
 */
public final class NetworkUtility {

    private static final String TAG = Utility.class.getSimpleName();

    private NetworkUtility() {

    }

    public static boolean isOnline(Context context) {
        boolean bReturn = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            bReturn = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected();
        } catch (Exception e) {
            bReturn = false;
        }
        return bReturn;
    }

    public static boolean isLocationServiceOn(Context context) {
        boolean bReturn = false;
        try {
            //LocationManager cm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            //bReturn = cm.isEnabled();
        } catch (Exception e) {
            bReturn = false;
        }
        return bReturn;
    }
}

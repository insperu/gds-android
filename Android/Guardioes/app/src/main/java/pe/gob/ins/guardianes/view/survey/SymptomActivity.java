package pe.gob.ins.guardianes.view.survey;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.analytics.HitBuilders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.AddressHelper;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DateFormat;
import pe.gob.ins.guardianes.helper.DialogBuilder;
import pe.gob.ins.guardianes.helper.Helper;
import pe.gob.ins.guardianes.helper.Mask;
import pe.gob.ins.guardianes.helper.SocialShare;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.Country;
import pe.gob.ins.guardianes.model.SymptomList;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.request.SurveyRequester;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.CountryAdapter;
import pe.gob.ins.guardianes.view.HomeActivity;
import pe.gob.ins.guardianes.view.ItemAdapter;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;
import pe.gob.ins.guardianes.view.dialog.LoadDialog;

public class SymptomActivity extends BaseAppCompatActivity {

    private static final int NONE = 0;
    final LoadDialog loadDialog = new LoadDialog();
    @BindView(R.id.list_view)
    ListView listView;

    private boolean isExantematica = false;
    private List<SymptomList> symptomArray = new ArrayList<>();
    private String id;
    private double latitude;
    private double longitude;
    private EditText startDate;
    private Spinner spCountry;
    private Spinner spState;
    private Spinner spProvince;
    private Spinner spCity;
    private List<Country> countries;
    private List<String> states;
    private List<String> provinces;
    private List<String> cities;
    private String selectedCountry;
    private String selectedState;
    private String selectedProvince;
    private String selectedCity;
    private boolean hasSelectedTravelOption;

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        hasSelectedTravelOption = false;
        selectedCountry = "";
        selectedCity = "";
        selectedProvince = "";
        selectedState = "";
        id = getIntent().getStringExtra("id_user");
        latitude = getIntent().getDoubleExtra("latitude", 0);
        longitude = getIntent().getDoubleExtra("longitude", 0);

        setContentView(R.layout.symptom);

        final View headerView = LayoutInflater.from(this).inflate(R.layout.symptom_header, null);

        startDate = headerView.findViewById(R.id.et_symptoms_start);
        startDate.addTextChangedListener(Mask.insert("##/##/####", startDate));

        listView.addHeaderView(headerView);

        View footerView = LayoutInflater.from(this).inflate(R.layout.symptom_footer, null);
        footerView.findViewById(R.id.button_confirm).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                if (areSurveyFieldsValid()) {
                    confirmSendSymptons();
                }
            }
        });

        listView.addFooterView(footerView);

        spCountry = listView.findViewById(R.id.spinner_country);
        spState = listView.findViewById(R.id.spinner_state);
        spCity = listView.findViewById(R.id.spinner_city);
        spProvince = listView.findViewById(R.id.spinner_province);
        countries = Helper.loadCountry();
        countries.add(new Country(0, "", getString(R.string.select)));
        CountryAdapter adapter = new CountryAdapter(this, CountryAdapter.Type.EDIT, countries);
        spCountry.setAdapter(adapter);
        Country peru = null;
        for (Country obj : adapter.getContries()) {
            if (obj.getName().equalsIgnoreCase("Perú"))
                peru = obj;
        }
        if (peru != null) {
            spCountry.setSelection(adapter.getPosition(peru));
        }
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateStateProvinceAndCity(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        new SurveyRequester(this).getSymptom(new RequestListener<String>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onError(final Exception e) {

            }

            @Override
            public void onSuccess(final String result) {

                try {

                    JSONObject jsonObject = new JSONObject(result);
                    ArrayList<SymptomList> symptoms = new ArrayList<SymptomList>();

                    if (!jsonObject.getBoolean("error")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        if (jsonArray.length() > 0) {


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObjectSymptoms = jsonArray.getJSONObject(i);

                                Log.d("Code", jsonObjectSymptoms.get("code").toString() + " ; " + jsonObjectSymptoms.get("name").toString());

                                SymptomList symptomList = new SymptomList(jsonObjectSymptoms.get("code").toString(), jsonObjectSymptoms.get("name").toString());
                                symptoms.add(symptomList);

                            }
                        }
                        Collections.sort(symptoms);
                        symptomArray.addAll(symptoms);
                        listView.setAdapter(new SymptomAdapter(SymptomActivity.this, symptomArray));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean areSurveyFieldsValid() {
        boolean areSurveyFieldsValid = isStartDateValid() && areSymptomsValid();
        if (areSurveyFieldsValid && hasSelectedTravelOption) {
            areSurveyFieldsValid = isTravelLocationValid();
        }
        return areSurveyFieldsValid;
    }

    private boolean isStartDateValid() {

        String startDate = this.startDate.getText().toString().trim().toLowerCase();

        boolean isStartDateValid = true;

        if (!DateFormat.isDate(startDate)) {
            new DialogBuilder(SymptomActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.invalid_start_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isStartDateValid = false;
        } else {
            if (isDateAfterToday(startDate)) {
                new DialogBuilder(SymptomActivity.this).load()
                        .title(R.string.attention)
                        .content(R.string.invalid_start_date_from_future_alert)
                        .positiveText(R.string.ok)
                        .show();
                isStartDateValid = false;
            }
        }

        return isStartDateValid;
    }

    private boolean isDateAfterToday(String startDate) {
        boolean isDateAfterToday = true;
        try {
            @SuppressLint("SimpleDateFormat")
            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
            long millisecondsFromNow = date.getTime() - (new Date()).getTime();
            Log.d("LOG", "R:" + millisecondsFromNow);
            if (millisecondsFromNow <= 0) {
                isDateAfterToday = false;
            }
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), e.getMessage());
        }
        return isDateAfterToday;
    }

    private boolean areSymptomsValid() {
        boolean areSymptomsValid = false;

        for (int i = 0; i < symptomArray.size(); i++) {
            if (symptomArray.get(i).isSelected()) {
                areSymptomsValid = true;
                break;
            }
        }

        if (!areSymptomsValid) {
            new DialogBuilder(SymptomActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.no_symptoms_selected_alert)
                    .positiveText(R.string.ok)
                    .show();
        }


        return areSymptomsValid;
    }

    private boolean isTravelLocationValid() {
        boolean isHomeLocationValid = true;

        if (((Country) spCountry.getSelectedItem()).getName().equals(getString(R.string.peru))) {
            isHomeLocationValid = isStateValid() && isProvinceValid() && isCityValid();
        }

        return isHomeLocationValid;
    }

    private boolean isStateValid() {
        boolean isStateValid = spState.getSelectedItemPosition() != NONE;

        if (!isStateValid) {
            new DialogBuilder(SymptomActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_travel_state_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isStateValid;
    }

    private boolean isProvinceValid() {
        boolean isProvinceValid = spProvince.getSelectedItemPosition() != NONE;

        if (!isProvinceValid) {
            new DialogBuilder(SymptomActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_travel_province_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isProvinceValid;
    }

    private boolean isCityValid() {

        boolean isCityValid = spCity.getSelectedItemPosition() != NONE;

        if (!isCityValid) {
            new DialogBuilder(SymptomActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_travel_city_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isCityValid;
    }

    private void updateStateProvinceAndCity(int i) {
        selectedCountry = countries.get(i).getName();
        boolean isCountryPeru = selectedCountry.equals(getString(R.string.peru));
        listView.findViewById(R.id.text_view_state).setVisibility(isCountryPeru ? View.VISIBLE : View.GONE);
        spState.setVisibility(isCountryPeru ? View.VISIBLE : View.GONE);
        if(isCountryPeru) {
            states = AddressHelper.getDepartments(
                    toList(getResources().getStringArray(R.array.departments_provinces_districts)));
            spState.setAdapter(
                    new ItemAdapter(this, states));
            spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    selectedState = states.get(i);
                    updateProvince();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
        listView.findViewById(R.id.text_view_province).setVisibility(isCountryPeru ? View.VISIBLE : View.GONE);
        spProvince.setVisibility(isCountryPeru ? View.VISIBLE : View.GONE);
        listView.findViewById(R.id.text_view_city).setVisibility(isCountryPeru ? View.VISIBLE : View.GONE);
        spCity.setVisibility(isCountryPeru ? View.VISIBLE : View.GONE);
    }

    private void updateProvince() {
        provinces = AddressHelper.getProvinces(toList(getResources().getStringArray(R.array.departments_provinces_districts)), selectedState);
        spProvince.setAdapter(
                new ItemAdapter(this, provinces));
        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedProvince = provinces.get(i);
                updateCity();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void updateCity() {
        cities = AddressHelper.getDistricts(
                toList(getResources().getStringArray(R.array.departments_provinces_districts)),
                selectedProvince);
        spCity.setAdapter(new ItemAdapter(this, cities));
        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = cities.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        getTracker().setScreenName("Symptom Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }


    private void sendSymptom() throws JSONException, ExecutionException, InterruptedException {
        loadDialog.show(getFragmentManager(), LoadDialog.TAG);
        final Map<String, Object> map = new HashMap<>();

        final User user = new User();

        user.setId(id);

        User savedUser = new PrefManager(SymptomActivity.this).get(Constants.Pref.USER, User.class);

        if (savedUser != null) {
            map.put("user_id", savedUser.getId());
            if (!(user.getId().equals(savedUser.getId()))) {
                map.put("household_id", user.getId());
            }
        }
        map.put("lat", latitude);
        map.put("lon", longitude);
        map.put("app_token", user.getAppToken());
        map.put("platform", user.getPlatform());
        map.put("client", user.getClient());
        map.put("no_symptom", "N");
        map.put("token", savedUser.getUserToken());
        String country = "";
        map.put("travelLocation", country);
        map.put("deviceModel", android.os.Build.MODEL + " (" + android.os.Build.PRODUCT + ")");
        map.put("startDate", DateFormat.getDate(startDate.getText().toString().trim().toLowerCase()));
        if (!selectedCountry.isEmpty()) {
            map.put("originCountry", selectedCountry);
            if (selectedCountry.equals(getString(R.string.peru))) {
                map.put("originState", selectedState);
                map.put("originProvince", selectedProvince);
                map.put("originCity", selectedCity);
            } else {
                map.put("originState", getString(R.string.na));
                map.put("originProvince", getString(R.string.na));
                map.put("originCity", getString(R.string.na));
            }
        } else {
            map.put("originCountry", getString(R.string.na));
            map.put("originState", getString(R.string.na));
            map.put("originProvince", getString(R.string.na));
            map.put("originCity", getString(R.string.na));
        }


        for (int i = 0; i < symptomArray.size(); i++) {
            String symptomName = symptomArray.get(i).getCodigo();
            Log.d("symptomArray(" + i + ")", symptomName);
            if (symptomArray.get(i).isSelected()) {
                if (symptomName.equals("hadContagiousContact") || symptomName.equals("hadHealthCare") || symptomName.equals("hadTravelledAbroad")) {
                    map.put(symptomArray.get(i).getCodigo(), "true");
                } else {
                    map.put(symptomArray.get(i).getCodigo(), "Y");
                }
            }
        }

        Log.d("SymptomActivity Map", map.toString());

        new SurveyRequester(this).sendSurvey(map, new RequestListener<String>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onError(final Exception e) {
                loadDialog.dismiss();
                new DialogBuilder(SymptomActivity.this).load()
                        .title(R.string.attention)
                        .content(e.getMessage() == null ? getString(R.string.error_send_survey) : e.getMessage())
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {

                            @Override
                            public void onClick(@NonNull final MaterialDialog dialog, @NonNull final DialogAction which) {
                                goHome();
                            }

                        }).show();
            }

            @Override
            public void onSuccess(final String result) {
                hasSurveyToday();
                loadDialog.dismiss();
                try {

                    final JSONObject jsonObject = new JSONObject(result);
                    Log.d("sendSurvey", jsonObject.toString());
                    if (jsonObject.get("error").toString() == "true") {
                        String error = jsonObject.get("message").toString();
                        new DialogBuilder(SymptomActivity.this).load()
                                .title(R.string.attention)
                                .content(error == null ? getString(R.string.error_send_survey) : error)
                                .positiveText(R.string.ok)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {

                                    @Override
                                    public void onClick(@NonNull final MaterialDialog dialog, @NonNull final DialogAction which) {
                                        navigateTo(HomeActivity.class);
                                    }

                                }).show();
                        Log.e("SymptonActivity", error);
                    } else {
                        if (jsonObject.get("exantematica").toString() == "true") {
                            isExantematica = true;
                        }
                    }
                    Log.d("ISEXANTEMATICA", "" + isExantematica);
                    if (isExantematica) {
                        Log.d("confirmSendSymptons", "navigateTo(ZikaActivity.class)");
                        navigateTo(ZikaActivity.class);
                    } else {
                        Log.d("confirmSendSymptons", "navigateTo(ShareActivity.class, bundle)");
                        final Bundle bundle = new Bundle();
                        bundle.putBoolean(Constants.Bundle.BAD_STATE, true);
                        navigateTo(ShareActivity.class, bundle);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void goHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (SocialShare.getInstance().isShared()) {
            new DialogBuilder(SymptomActivity.this).load()
                    .title(R.string.app_name)
                    .content(R.string.share_ok)
                    .positiveText(R.string.ok)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(final MaterialDialog dialog) {
                            SocialShare.getInstance().setIsShared(false);
                            navigateTo(HomeActivity.class);
                        }
                    }).show();
        }
    }

    private void confirmSendSymptons() {
        new DialogBuilder(SymptomActivity.this).load()
                .title(R.string.attention)
                .content(R.string.message_register_info)
                .negativeText(R.string.no)
                .positiveText(R.string.yes)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(final MaterialDialog dialog) {
                        try {
                            sendSymptom();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).show();
    }

    private void hasSurveyToday() {

        new SurveyRequester(this).hasSurveyToday(new RequestListener<Integer>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onError(final Exception e) {

            }

            @Override
            public void onSuccess(final Integer amount) {
                Log.d("hasSurveyToday", "" + amount);
                if (amount == 0) {

                    final User user = new PrefManager(SymptomActivity.this).get(Constants.Pref.USER, User.class);

                    if (user != null) {

                        user.addEnergy(10);

                        new PrefManager(SymptomActivity.this).put(Constants.Pref.USER, user);
                    }
                    Log.d("hasSurveyToday.user", "" + user.toString());
                }
            }
        });
    }

    private List<String> toList(final String[] valueArray) {

        final List<String> valueList = new LinkedList<>(Arrays.asList(valueArray));

        valueList.add(0, getString(R.string.select));

        return valueList;
    }


    public void makeLocationVisible() {
        hasSelectedTravelOption = true;
        listView.findViewById(R.id.ll_travel_location).setVisibility(View.VISIBLE);
        listView.findViewById(R.id.text_view_country).setVisibility(View.VISIBLE);
        spCountry.setVisibility(View.VISIBLE);
    }

    public void makeLocationGone() {
        hasSelectedTravelOption = false;
        listView.findViewById(R.id.ll_travel_location).setVisibility(View.GONE);
        listView.findViewById(R.id.text_view_country).setVisibility(View.GONE);
        spCountry.setVisibility(View.GONE);
        listView.findViewById(R.id.text_view_city).setVisibility(View.GONE);
        spCity.setVisibility(View.GONE);
        listView.findViewById(R.id.text_view_state).setVisibility(View.GONE);
        spState.setVisibility(View.GONE);
    }
}

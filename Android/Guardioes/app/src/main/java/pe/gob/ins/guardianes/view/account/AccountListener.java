package pe.gob.ins.guardianes.view.account;

import pe.gob.ins.guardianes.model.User;

/**
 * @author Igor Morais
 */
public interface AccountListener {

    void onCancel();

    void onError();

    void onNotFound(User user);

    void onSuccess(User user);
}

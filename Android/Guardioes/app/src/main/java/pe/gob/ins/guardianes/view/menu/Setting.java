package pe.gob.ins.guardianes.view.menu;

import pe.gob.ins.guardianes.R;

/**
 * @author Igor Morais
 */
public enum Setting {

    ALERT (R.string.alert),
    SOUND (R.string.sound),
    EMAIL (R.string.mails);

    private final int name;

    Setting(final int name) {
        this.name = name;
    }

    public int getName() {
        return name;
    }
}

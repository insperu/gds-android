package pe.gob.ins.guardianes.view.tip;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class PhoneActivity extends BaseAppCompatActivity {
    int phoneId;

    @BindView(R.id.button_call)
    Button buttonCall;

    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.phone);

        phoneId = getIntent().getIntExtra("phone_id", 1);
        TextView textViewDesc = findViewById(R.id.text_call_desc);
        TextView textViewNumber = findViewById(R.id.text_call_number);

        Phone phone = Phone.getBy(phoneId);
        textViewDesc.setText(phone.getName());
        textViewNumber.setText(phone.getNumber());
    }

    @OnClick(R.id.button_call)
    public void onClick() {

        Uri uri = Uri.parse("tel:" + Phone.getBy(phoneId).getNumber());

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(uri);
        startActivity(intent);
    }
}

package pe.gob.ins.guardianes.model;

public enum PointType {

    HOSPITAL, PHARMACY;
}

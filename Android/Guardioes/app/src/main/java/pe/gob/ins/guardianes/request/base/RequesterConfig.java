package pe.gob.ins.guardianes.request.base;

public final class RequesterConfig {

    public static final int TIMEOUT = 25;
    private static final String ADDRESS = "http://190.102.152.94"; // External
    //private static final String ADDRESS = "http://192.168.128.75"; // Internal
    private static final String ADDRESS_DEV = ADDRESS; // External
    private static final String PORT = "";
    private static final String CONTEXT = "";
    public static final String URL_PROD = ADDRESS + CONTEXT;
    public static final String URL_DEV = ADDRESS_DEV + CONTEXT;
    public static final String URL = URL_DEV;

    public static class Api {

        public static final String USER = "/user";
        public static final String NOTICE = "/news";
    }
}

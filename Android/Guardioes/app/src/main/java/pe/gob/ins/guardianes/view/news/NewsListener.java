package pe.gob.ins.guardianes.view.news;

import pe.gob.ins.guardianes.model.News;

interface NewsListener {

    void onNewsSelect(News news);
}

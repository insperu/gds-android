package pe.gob.ins.guardianes.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class AddressHelper {

    private static final String DIVISOR = " >> ";

    public static List<String> getDepartments(List<String> data){
        ArrayList<String> departments = new ArrayList<>();
        String emptyOption = data.get(0);
        data.remove(0);
        for(String dpd : data) {
            String department = dpd.split(DIVISOR)[0];
            if(!departments.contains(department)) {
                departments.add(department);
            }
        }
        Collections.sort(departments);
        departments.add(0, emptyOption);
        return departments;
    }

    public static List<String> getProvinces(List<String> data, String department) {
        ArrayList<String> provinces = new ArrayList<>();
        String emptyOption = data.get(0);
        data.remove(0);
        for(String dpd : data) {
            String[] split = dpd.split(DIVISOR);
            String currentDepartment = split[0];
            if(currentDepartment.equals(department)) {
                String province = split[1];
                if(!provinces.contains(province)) {
                    provinces.add(province);
                }
            }
        }
        Collections.sort(provinces);
        provinces.add(0, emptyOption);
        return provinces;
    }

    public static List<String> getDistricts(List<String> data, String province) {
        ArrayList<String> districts = new ArrayList<>();
        String emptyOption = data.get(0);
        data.remove(0);
        for(String dpd : data) {
            String[] split = dpd.split(DIVISOR);
            String currentProvince = split[1];
            if(currentProvince.equals(province)) {
                String district = split[2];
                if(!districts.contains(district)) {
                    districts.add(district);
                }
            }
        }
        Collections.sort(districts);
        districts.add(0, emptyOption);
        return districts;
    }
}

package pe.gob.ins.guardianes.helper;

/**
 * @author Igor Morais
 */
public final class Constants {

    public static final int MAX_USER = 10;
    public static final String PATH = "file://";
    public static final String DIRECTORY_TEMP = "Guardioes";

    private Constants() {

    }

    public static class Bundle {

        public static final String WELCOME = "Welcome";
        public static final String TIP = "Tip";
        public static final String MAIN_MEMBER = "main_member";
        public static final String BAD_STATE = "has_bad_state";
        public static final String NEW_MEMBER = "new_member";
        public static final String SOCIAL_NEW = "social_new";
        public static final String TYPE = "TYPE";
        public static final String PATH = "Path";
        public static final String DELETE = "Delete";
        public static final String NAME = "Name";
        public static final String USER = "User";
    }

    public static class Pref {

        public static final String USER = "user";
        public static final String LANGUAGE = "Language";
        public static final String PREFS_NAME = "preferences_user_token";
        public static final String GCM_TOKEN_VERSION = "new_gcm_token";
    }
}

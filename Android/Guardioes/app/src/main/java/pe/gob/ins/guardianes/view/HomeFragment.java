package pe.gob.ins.guardianes.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;

import butterknife.BindView;
import butterknife.OnClick;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.AvatarHelper;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.view.base.BaseFragment;
import pe.gob.ins.guardianes.view.diary.DiaryActivity;
import pe.gob.ins.guardianes.view.news.NewsActivity;
import pe.gob.ins.guardianes.view.survey.SelectParticipantActivity;
import pe.gob.ins.guardianes.view.tip.TipActivity;

/**
 * @author Igor Morais
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.text_view_name)
    TextView textViewName;
    @BindView(R.id.home_report)
    ImageView imageViewReport;
    @BindView(R.id.text_view_quest)
    TextView textViewQuest;

    private User user = null;

    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup viewGroup, Bundle bundle) {
        return inflater.inflate(R.layout.home_fragment, viewGroup, false);
    }

    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);

        setShowTitle(false);
        setShowLogo(true);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle bundle) {
        user = new PrefManager(getActivity()).get(Constants.Pref.USER, User.class);
        bind(view);
        textViewName.setText(getString(R.string.message_hello, user.getFirstname()));
    }

    @Override
    public void onResume() {
        super.onResume();
        imageViewReport.setOnClickListener(this);
        textViewQuest.setOnClickListener(this);
    }

    @OnClick(R.id.btn_news)
    public void onNotice() {
        navigateTo(NewsActivity.class);
    }

    @OnClick(R.id.btn_map)
    public void onMap() {
        navigateTo(MapSymptomActivity.class);
    }

    @OnClick(R.id.btn_tip)
    public void onTip() {
        navigateTo(TipActivity.class);
    }

    @OnClick(R.id.btn_diary)
    public void onDiary() {
        navigateTo(DiaryActivity.class);
    }

    @OnClick(R.id.btn_participe)
    public void onJoin() {
        navigateTo(SelectParticipantActivity.class);
    }

    @Override
    public void onClick(View view) {
        navigateTo(SelectParticipantActivity.class);
    }
}

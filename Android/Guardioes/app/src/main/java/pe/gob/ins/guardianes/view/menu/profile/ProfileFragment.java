package pe.gob.ins.guardianes.view.menu.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.analytics.HitBuilders;

import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DialogBuilder;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.request.UserRequester;
import pe.gob.ins.guardianes.request.base.Method;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.request.old.Requester;
import pe.gob.ins.guardianes.request.old.SimpleRequester;
import pe.gob.ins.guardianes.view.base.BaseFragment;
import pe.gob.ins.guardianes.view.dialog.LoadDialog;
import pe.gob.ins.guardianes.view.menu.UserActivity;

public class ProfileFragment extends BaseFragment implements UserListener {

    @BindView(R.id.list_view)
    ListView listView;

    private User user = null;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup viewGroup, final Bundle bundle) {
        return inflater.inflate(R.layout.profile, viewGroup, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle bundle) {
        user = new PrefManager(getActivity()).get(Constants.Pref.USER, User.class);
        bind(view);
    }

    @Override
    public void onResume() {
        super.onResume();

        User user = new PrefManager(getActivity()).get(Constants.Pref.USER, User.class);

        new UserRequester(getActivity()).getAllProfiles(user.getId(), new UserHandler());
    }

    @Override
    @OnClick(R.id.button_save)
    public void onAdd() {

        if (listView.getAdapter().getCount() == Constants.MAX_USER) {

            new DialogBuilder(getActivity()).load()
                    .title(R.string.app_name)
                    .content(R.string.message_user_max)
                    .positiveText(R.string.ok)
                    .show();

        } else {

            Bundle bundle = new Bundle();

            bundle.putBoolean(Constants.Bundle.MAIN_MEMBER, false);
            bundle.putBoolean(Constants.Bundle.NEW_MEMBER, true);

            navigateTo(UserActivity.class);
        }
    }

    @Override
    public void onEdit(final User user) {

        getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Edit Profile Button")
                .build());

        final Bundle bundle = new Bundle();

        bundle.putParcelable(Constants.Bundle.USER, Parcels.wrap(user));

        if (this.user.getId().equals(user.getId())) {
            bundle.putBoolean(Constants.Bundle.MAIN_MEMBER, true);
        }

        navigateTo(UserActivity.class, bundle);
    }

    @Override
    public void onDelete(final User user) {
        getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Delete Member Button")
                .build());

        if (this.user.getId() == user.getId()) {

            new DialogBuilder(getActivity()).load()
                    .title(R.string.attention)
                    .content(R.string.not_remove_member)
                    .positiveText(R.string.ok)
                    .show();
        } else {

            new DialogBuilder(getActivity()).load()
                    .title(R.string.attention)
                    .content(R.string.delete_profile)
                    .positiveText(R.string.yes)
                    .negativeText(R.string.no)
                    .callback(new MaterialDialog.ButtonCallback() {

                        @Override
                        public void onNegative(final MaterialDialog dialog) {

                        }

                        @Override
                        public void onPositive(final MaterialDialog dialog) {

                            SimpleRequester simpleRequester = new SimpleRequester();
                            simpleRequester.setMethod(Method.GET);
                            simpleRequester.setUrl(Requester.API_URL + "household/delete/" + user.getId() + "?client=api");
                            simpleRequester.setJsonObject(null);

                            try {
                                String jsonStr = simpleRequester.execute(simpleRequester).get();

                                JSONObject jsonObject = new JSONObject(jsonStr);

                                if (jsonObject.get("error").toString() == "true") {
                                    refresh(true);
                                } else {
                                    refresh(false);
                                }

                            } catch (Exception e) {
                                Toast.makeText(getActivity(), R.string.generic_error + " - " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }).show();
        }
    }

    private void refresh(boolean error) {

        if (error) {

            Toast.makeText(getActivity(), R.string.generic_error, Toast.LENGTH_SHORT).show();

        } else {

            Toast.makeText(getActivity(), R.string.change_it_urgently, Toast.LENGTH_SHORT).show();

            new UserRequester(getActivity()).getAllProfiles(user.getId(), new UserHandler());
        }
    }

    private class UserHandler implements RequestListener<List<User>> {

        private final LoadDialog loadDialog = new LoadDialog();

        @Override
        public void onStart() {
            loadDialog.show(getFragmentManager(), LoadDialog.TAG);
        }

        @Override
        public void onError(Exception e) {

        }

        @Override
        public void onSuccess(final List<User> parentList) {
            loadDialog.dismiss();

            parentList.add(0, user);

            listView.setAdapter(new UserAdapter(getActivity(), parentList, ProfileFragment.this));
        }
    }
}

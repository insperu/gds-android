package pe.gob.ins.guardianes.view.account;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.analytics.HitBuilders;

import butterknife.BindView;
import butterknife.OnClick;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DialogBuilder;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.push.HashReceiver;
import pe.gob.ins.guardianes.push.RegisterService;
import pe.gob.ins.guardianes.request.UserRequester;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.HomeActivity;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;
import pe.gob.ins.guardianes.view.dialog.LoadDialog;

public class LoginActivity extends BaseAppCompatActivity {

    private static final String SOCIAL_FRAGMENT = "social_fragment";

    @BindView(R.id.linear_layout_social_login)
    LinearLayout linearLayoutSocialLogin;

    @BindView(R.id.linear_layout_login)
    LinearLayout linearLayoutLogin;

    @BindView(R.id.edit_text_mail)
    EditText etEmail;

    @BindView(R.id.edit_text_password)
    EditText etPassword;

    private SocialFragment socialFragment;

    private boolean inLogin;

    private boolean socialLogin;

    private User user;

    private final LoadDialog loadDialog = new LoadDialog();

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.login);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSocialFragment().setLogin(true);
        getSocialFragment().setListener(new AccountHandler());
    }

    @Override
    public void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(HashReceiver.HASH_RECEIVER));

        getTracker().setScreenName("Login Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            if (inLogin) {
                handlerAnimation();

            } else {

                return super.onOptionsItemSelected(item);
            }

        } else {

            return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onBackPressed() {

        if (inLogin) {
            handlerAnimation();

        } else {
            super.onBackPressed();
        }
    }

    @OnClick(R.id.text_view_forgot_password)
    public void onForgotPassword() {

        getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Forgot Password Button")
                .build());

        navigateTo(ForgotPasswordActivity.class);
    }

    @OnClick(R.id.button_mail)
    public void onLoginAnimation() {

        final Animation slideIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);

        slideIn.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(final Animation animation) {
                linearLayoutLogin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                inLogin = true;

                linearLayoutSocialLogin.setVisibility(View.INVISIBLE);

                linearLayoutSocialLogin.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(final Animation animation) {

            }
        });

        final Animation slideOut = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);

        linearLayoutLogin.startAnimation(slideIn);
        linearLayoutSocialLogin.startAnimation(slideOut);
    }

    private void handlerAnimation() {

        final Animation slideIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);

        slideIn.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(final Animation animation) {
                linearLayoutSocialLogin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                inLogin = false;

                linearLayoutLogin.setVisibility(View.INVISIBLE);

                linearLayoutLogin.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(final Animation animation) {

            }
        });

        final Animation slideOut = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);

        linearLayoutSocialLogin.startAnimation(slideIn);
        linearLayoutLogin.startAnimation(slideOut);
    }

    @OnClick(R.id.button_login)
    public void onLogin() {

        if (areAllFieldsValid()) {
            loadDialog.show(getFragmentManager(), LoadDialog.TAG);

            startService(new Intent(LoginActivity.this, RegisterService.class));
        }
    }

    private boolean areAllFieldsValid() {

        return isEmailValid() && isPasswordValid();
    }

    private boolean isEmailValid() {
        String email = etEmail.getText().toString().toLowerCase();

        boolean isEmailValid = true;

        if (email.isEmpty()) {
            new DialogBuilder(LoginActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_email_alert)
                    .positiveText(R.string.ok)
                    .show();
            isEmailValid = false;
        } else {
            try {
                if (email.split("@")[1].split("\\.").length < 2) {
                    isEmailValid = false;
                }
            } catch (Exception e) {
                isEmailValid = false;
            }

            if (!isEmailValid) {
                new DialogBuilder(LoginActivity.this).load()
                        .title(R.string.attention)
                        .content(R.string.invalid_email_alert)
                        .positiveText(R.string.ok)
                        .show();
            }
        }

        return isEmailValid;
    }

    private boolean isPasswordValid() {
        String password = etPassword.getText().toString();

        boolean isPasswordValid = true;

        if (password.length() < 6) {
            new DialogBuilder(LoginActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.invalid_password_alert)
                    .positiveText(R.string.ok)
                    .show();
            isPasswordValid = false;
        }

        return isPasswordValid;
    }

    private class AccountHandler implements AccountListener {

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }

        @Override
        public void onNotFound(final User user) {

            new DialogBuilder(LoginActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.change_it_urgently)
                    .positiveText(R.string.ok)
                    .show();
        }

        @Override
        public void onSuccess(final User user) {

            socialLogin = true;

            setUser(user);
            try {
                loadDialog.show(getFragmentManager(), LoadDialog.TAG);
            } catch (Exception e) {
                e.printStackTrace();
            }
            startService(new Intent(LoginActivity.this, RegisterService.class));
        }
    }

    public void setUser(final User user) {
        this.user = user;
    }

    private SocialFragment getSocialFragment() {

        if (socialFragment == null) {
            socialFragment = (SocialFragment) getFragmentManager().findFragmentByTag(SOCIAL_FRAGMENT);
        }

        return socialFragment;
    }

    private HashReceiver receiver = new HashReceiver() {

        public void onHash(final String hash) {

            final String mail = socialLogin ? user.getEmail() : etEmail.getText().toString().toLowerCase().trim();
            final String password = socialLogin ? user.getPassword() : etPassword.getText().toString().trim();

            new UserRequester(LoginActivity.this).login(mail, password, hash, new RequestListener<User>() {

                @Override
                public void onStart() {

                }

                @Override
                public void onError(final Exception e) {
                    loadDialog.dismiss();

                    new DialogBuilder(LoginActivity.this).load()
                            .title(R.string.attention)
                            .content(R.string.generic_error_login)
                            .positiveText(R.string.ok)
                            .show();
                }

                @Override
                public void onSuccess(final User user) {
                    loadDialog.dismiss();

                    new PrefManager(LoginActivity.this).put(Constants.Pref.USER, user);

                    navigateTo(HomeActivity.class, Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_NEW_TASK);
                }
            });
        }
    };

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        loadDialog.dismiss();
        getSocialFragment().onActivityResult(requestCode, resultCode, intent);
    }
}

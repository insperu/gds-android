package pe.gob.ins.guardianes.view.menu.profile;

import pe.gob.ins.guardianes.model.User;

/**
 * @author Igor Morais
 */
public interface UserListener {

    void onAdd();

    void onEdit(User user);

    void onDelete(User user);

    // TODO: We need to define the entity, this ia a stub only
}

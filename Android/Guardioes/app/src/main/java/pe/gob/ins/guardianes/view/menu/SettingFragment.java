package pe.gob.ins.guardianes.view.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import pe.gob.ins.guardianes.R;

import pe.gob.ins.guardianes.view.base.BaseFragment;

import butterknife.BindView;

/**
 * @author Igor Morais
 */
public class SettingFragment extends BaseFragment {

    @BindView(R.id.list_view)
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup viewGroup, final Bundle bundle) {
        return inflater.inflate(R.layout.setting, viewGroup, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle bundle) {

        bind(view);

        listView.setAdapter(new SettingAdapter(getActivity(), Setting.values()));
    }

    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);

        getSupportActionBar().setTitle(R.string.setting);
    }
}

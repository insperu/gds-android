package pe.gob.ins.guardianes.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DialogBuilder;
import pe.gob.ins.guardianes.manager.Loader;
import pe.gob.ins.guardianes.model.HealthCenterPoint;
import pe.gob.ins.guardianes.model.Point;
import pe.gob.ins.guardianes.request.MapRequester;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.base.AbstractBaseMapActivity;
import pe.gob.ins.guardianes.view.tip.Tip;

/**
 * @author Igor Morais / Miquéias Lopes
 */
public class MapPointActivity extends AbstractBaseMapActivity {

    @BindView(R.id.linear_layout)
    LinearLayout linearLayoutPoint;

    @BindView(R.id.text_view_name)
    TextView textViewName;

    @BindView(R.id.text_view_address)
    TextView textViewAddress;

    private MarkerOptions markerOption;

    private final Map<Marker, Point> pointMap = new HashMap<>();
    private final Map<Marker, HealthCenterPoint> hcPointMap = new HashMap<>();

    private Tip tip;
    private Point point;
    private HealthCenterPoint hcPoint;

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.map_point);

        tip = Tip.getBy(getIntent().getIntExtra(Constants.Bundle.TIP, 0));

        final MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.fragment_map);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        super.onMapReady(map);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (tip == Tip.PHARMACY) {
            getTracker().setScreenName("Pharmacy Screen - " + this.getClass().getSimpleName());

        } else {
            getTracker().setScreenName("UPAs Screen - " + this.getClass().getSimpleName());
        }

        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onAnimationEnd(final LatLng latLng) {

        if (tip == Tip.HOSPITAL) {
            load();

        } else {
            loadPharmacy(latLng.latitude, latLng.longitude);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadPharmacy(final double latitude, final double longitude) {

        new MapRequester(this).loadPharmacy(latitude, longitude, new RequestListener<List<Point>>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onError(final Exception e) {

            }

            @Override
            public void onSuccess(final List<Point> pointList) {
                addPharmacyMarker(pointList);
            }


        });
    }

    private void load() {

        Loader.with().handler().post(new Runnable() {

            @Override
            public void run() {

                try {

                    final InputStream inputStream = getAssets().open("health_units.json");

                    final List<HealthCenterPoint> pointList = new ObjectMapper().readValue(inputStream, new TypeReference<List<HealthCenterPoint>>() {
                    });

                    addHealthCenterMarker(pointList);

                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addPharmacyMarker(final List<Point> pointList) {

        final Handler handler = new Handler(Looper.getMainLooper());

        for (final Point point : pointList) {

            handler.post(new Runnable() {

                @Override
                public void run() {

                    final LatLng latLng = new LatLng(point.getLatitude(), point.getLongitude());

                    final Marker marker = getMap().addMarker(getMarkerOption().position(latLng));

                    pointMap.put(marker, point);
                }
            });
        }
    }

    private void addHealthCenterMarker(final List<HealthCenterPoint> pointList) {


        final Handler handler = new Handler(Looper.getMainLooper());

        for (final HealthCenterPoint point : pointList) {

            handler.post(new Runnable() {

                @Override
                public void run() {

                    final LatLng latLng = new LatLng(point.getLatitude(), point.getLongitude());

                    final Marker marker = getMap().addMarker(getMarkerOption().position(latLng));
                    System.out.println(marker.getPosition().toString());

                    hcPointMap.put(marker, point);
                }
            });
        }
    }

    private MarkerOptions getMarkerOption() {

        if (tip == Tip.PHARMACY) {

            if (markerOption == null) {
                markerOption = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_pharmacy));
            }

        } else if (tip == Tip.HOSPITAL) {

            if (markerOption == null) {
                markerOption = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_hospital));
            }
        }

        return markerOption;
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        point = pointMap.get(marker);
        hcPoint = hcPointMap.get(marker);

        if (point != null || hcPoint != null) {

            if (tip == Tip.PHARMACY) {
                textViewName.setText(point.getName());
                textViewAddress.setText(format(point));
            } else {
                textViewName.setText(hcPoint.getName());
                textViewAddress.setText(format(hcPoint));
            }

            final Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in_top);

            animation.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(final Animation animation) {

                    if (linearLayoutPoint.getVisibility() == View.INVISIBLE) {
                        linearLayoutPoint.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onAnimationEnd(final Animation animation) {

                }

                @Override
                public void onAnimationRepeat(final Animation animation) {

                }
            });

            linearLayoutPoint.startAnimation(animation);
        }
        return true;
    }

    @OnClick(R.id.iv_mp_route)
    void onRouteClick() {
        new DialogBuilder(MapPointActivity.this).load()
                .title(R.string.attention)
                .content(R.string.open_google_maps)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onNegative(final MaterialDialog dialog) {

                    }

                    @Override
                    public void onPositive(final MaterialDialog dialog) {
                        double latitude;
                        double longitude;
                        String label;
                        if (tip == Tip.PHARMACY) {
                            latitude = point.getLatitude();
                            longitude = point.getLongitude();
                            label = point.getName();
                        } else {
                            latitude = hcPoint.getLatitude();
                            longitude = hcPoint.getLongitude();
                            label = hcPoint.getName();
                        }
                        String uriBegin = "geo:" + latitude + "," + longitude;
                        String query = latitude + "," + longitude + "(" + label + ")";
                        String uriString = uriBegin + "?q=" + Uri.encode(query) + "&z=14";
                        Uri uri = Uri.parse(uriString);
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }

                }).show();
    }

    private String format(final Point point) {
        String address = "";
        if (point.getLogradouro() != null) {
            address += point.getLogradouro();
        }
        if (point.getNumero() != null && !point.getNumero().equals("null")) {
            address += ", " + point.getNumero();
        }
        return address;
    }

    private String format(final HealthCenterPoint point) {
        String address = "";
        if (point.getAddress() != null) {
            address += point.getAddress();
        }
        if (point.getCity() != null) {
            address += ", " + point.getCity();
        }
        if (point.getState() != null) {
            address += " - " + point.getState();
        }
        return address;
    }
}

package pe.gob.ins.guardianes.view.survey;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.analytics.HitBuilders;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.AvatarHelper;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DateFormat;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.request.UserRequester;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;
import pe.gob.ins.guardianes.view.dialog.LoadDialog;
import pe.gob.ins.guardianes.view.menu.UserActivity;

public class SelectParticipantActivity extends BaseAppCompatActivity implements ParentListener {

    @BindView(R.id.text_view_name)
    TextView textViewName;

    @BindView(R.id.text_view_age)
    TextView textViewAge;

    @BindView(R.id.image_view_image)
    CircularImageView imageViewAvatar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private User user;

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.select_participant);

        user = new PrefManager(SelectParticipantActivity.this).get(Constants.Pref.USER, User.class);

        int j = DateFormat.getDateDiff(user.getDob());

        textViewName.setText(user.getFirstname());
        textViewAge.setText(j + " " + this.getString(R.string.anos));

        new AvatarHelper().loadImage(this, imageViewAvatar, user);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        loadHousehold();
    }

    private void loadHousehold() {

        final String novo_integrante = this.getString(R.string.adicionar_novo_integrante);

        new UserRequester(this).getAllHousehold(user.getId(), new RequestListener<List<User>>() {

            final LoadDialog loadDialog = new LoadDialog();

            @Override
            public void onStart() {
                loadDialog.show(getFragmentManager(), LoadDialog.TAG);
            }

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onSuccess(final List<User> parentList) {
                loadDialog.dismiss();

                parentList.add(new User("", "    " + novo_integrante, "", "-1", "", "", "", R.drawable.img_add_profile));

                recyclerView.setAdapter(new ParentAdapter(SelectParticipantActivity.this, SelectParticipantActivity.this, parentList));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        getTracker().setScreenName("Select Participant Survey Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }

    @OnClick(R.id.image_view_image)
    public void onUserSelect() {

        getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Survey Select Main User Button")
                .build());

        final Bundle bundle = new Bundle();

        bundle.putBoolean(Constants.Bundle.MAIN_MEMBER, true);
        navigateTo(StateActivity.class, bundle);
    }

    @Override
    public void onParentSelect(String id) {

        if (id.equals("-1")) {

            final Bundle bundle = new Bundle();

            bundle.putBoolean(Constants.Bundle.MAIN_MEMBER, false);
            bundle.putBoolean(Constants.Bundle.NEW_MEMBER, true);

            navigateTo(UserActivity.class, bundle);

        } else {
            final Bundle bundle = new Bundle();

            bundle.putString("id_user", id);
            bundle.putBoolean(Constants.Bundle.NEW_MEMBER, false);
            navigateTo(StateActivity.class, bundle);
        }
    }
}

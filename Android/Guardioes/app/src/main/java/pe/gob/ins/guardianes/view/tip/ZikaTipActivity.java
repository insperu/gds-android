package pe.gob.ins.guardianes.view.tip;

import android.os.Bundle;
import android.widget.TextView;

import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;
import com.google.android.gms.analytics.HitBuilders;

import butterknife.BindView;

/**
 * @author Miqueias Lopes
 */
public class ZikaTipActivity extends BaseAppCompatActivity {

    @BindView(R.id.zika_content)
    TextView zikaContent;

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.zika_info);
    }

    @Override
    public void onResume() {
        super.onResume();

        getTracker().setScreenName("Zika Tip Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }
}

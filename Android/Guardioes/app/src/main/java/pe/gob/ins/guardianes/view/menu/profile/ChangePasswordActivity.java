package pe.gob.ins.guardianes.view.menu.profile;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.request.UserRequester;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.dialog.LoadDialog;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_old_password)
    EditText editOldPassword;

    @BindView(R.id.edit_text_password)
    EditText edtNewPassword;

    @BindView(R.id.edit_text_confirm_password)
    EditText edtConfirmNewPassword;

    private User user;
    private final LoadDialog loadDialog = new LoadDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_activity);
        ButterKnife.bind(this);

        this.user = Parcels.unwrap(getIntent().getParcelableExtra(Constants.Bundle.USER));
    }

    private boolean isValid(String oldPassword, String newPassword, String confirmNewPassword){
        if (newPassword.length() < 6){
            return false;
        }

        if (newPassword.length() < 6){
            return false;
        }

        if (!newPassword.equals(confirmNewPassword)){
            Toast.makeText(this, getResources().getString(R.string.password_not_equal), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    @OnClick(R.id.button_redfine_password)
    public void onChangePassword(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        String oldPassword = this.editOldPassword.getText().toString();
        String password = this.edtNewPassword.getText().toString();
        String confirmPassword = this.edtConfirmNewPassword.getText().toString();
        loadDialog.show(getFragmentManager(), LoadDialog.TAG);

        if (this.isValid(oldPassword, password, confirmPassword)){
            new UserRequester(this).changePassword(this.user, oldPassword, password, new RequestListener<String>() {
                @Override
                public void onStart() {

                }

                @Override
                public void onError(Exception e) {
                    loadDialog.dismiss();
                }

                @Override
                public void onSuccess(String type) {
                    loadDialog.dismiss();
                    finish();
                }
            });
        }
    }
}

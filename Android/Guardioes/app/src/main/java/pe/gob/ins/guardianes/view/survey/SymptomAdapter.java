package pe.gob.ins.guardianes.view.survey;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import pe.gob.ins.guardianes.R;

import pe.gob.ins.guardianes.model.SymptomList;

import java.util.List;

public class SymptomAdapter extends ArrayAdapter<SymptomList> {

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_OTHER = 2;

    private static final int VIEW_TYPE_AMOUNT = 2;
    final private List<SymptomList> symptomArray;
    private int dynamicItensLengh;
    private SymptomActivity symptomActivity;


    public SymptomAdapter(SymptomActivity symptomActivity, final List<SymptomList> symptomArray) {
        super(symptomActivity, 0, symptomArray);
        this.symptomActivity = symptomActivity;

        this.symptomArray = symptomArray;
        this.dynamicItensLengh = this.symptomArray.size();

        this.symptomArray.add(new SymptomList("hadContagiousContact", symptomActivity.getString(R.string.symptom_contato)));
        this.symptomArray.add(new SymptomList("hadHealthCare", symptomActivity.getString(R.string.symptom_procureiservicosaude)));
        this.symptomArray.add(new SymptomList("hadTravelledAbroad", symptomActivity.getString(R.string.symptom_estivefora)));
    }

    @Override
    public int getCount() {
        return symptomArray.size() + 1;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_AMOUNT;
    }

    @Override
    public int getItemViewType(final int position) {
        return (position == dynamicItensLengh ? VIEW_TYPE_OTHER : VIEW_TYPE_ITEM) - 1;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup viewGroup) {
        View view = convertView;

        if (getItemViewType(position) == VIEW_TYPE_OTHER - 1) {

            View inflatedView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.symptom_other, viewGroup, false);

            return inflatedView;

        } else {

            final ViewHolder viewHolder;

            if (view == null) {

                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.symptom_item, viewGroup, false);

                viewHolder = new ViewHolder();

                viewHolder.checkBoxSymptom = (CheckBox) view.findViewById(R.id.check_box_symptom);

                view.setTag(viewHolder);
                view.setTag(R.id.check_box_symptom, viewHolder.checkBoxSymptom);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            final SymptomList symptomList = position > this.dynamicItensLengh ? this.symptomArray.get(position - 1) : this.symptomArray.get(position);

            if (position > this.dynamicItensLengh) {
                //viewHolder.textViewName.setTag(position - 1);
                viewHolder.checkBoxSymptom.setTag(position - 1);
            } else {
                //viewHolder.textViewName.setTag(position);
                viewHolder.checkBoxSymptom.setTag(position);
            }

            if (symptomList.getCodigo().equals("hadTravelledAbroad")) {
                viewHolder.checkBoxSymptom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        int getPosition = (Integer) buttonView.getTag();  // Here we get the position that we have set for the checkbox using setTag.
                        symptomArray.get(getPosition).setSelected(buttonView.isChecked()); // Set the value of checkbox to maintain its state.

                        if (isChecked) {
                            symptomActivity.makeLocationVisible();
                        } else {
                            symptomActivity.makeLocationGone();
                        }
                    }
                });
            } else {
                viewHolder.checkBoxSymptom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        int getPosition = (Integer) buttonView.getTag();  // Here we get the position that we have set for the checkbox using setTag.
                        symptomArray.get(getPosition).setSelected(buttonView.isChecked()); // Set the value of checkbox to maintain its state.
                    }
                });
            }

            //viewHolder.textViewName.setText(symptomList.getNome());
            viewHolder.checkBoxSymptom.setChecked(symptomList.isSelected());
            viewHolder.checkBoxSymptom.setText("" + symptomList.getNome() + "");
            viewHolder.checkBoxSymptom.setTextColor(Color.parseColor("#757575"));
            viewHolder.checkBoxSymptom.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            viewHolder.checkBoxSymptom.setCompoundDrawablePadding(50);
            viewHolder.checkBoxSymptom.setPadding(50, 0, 100, 0);


            return view;
        }
    }

    public static class ViewHolder {
        CheckBox checkBoxSymptom;
    }
}

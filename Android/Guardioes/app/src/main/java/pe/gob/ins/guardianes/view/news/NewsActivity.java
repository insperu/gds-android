package pe.gob.ins.guardianes.view.news;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import butterknife.BindView;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.DialogBuilder;
import pe.gob.ins.guardianes.model.News;
import pe.gob.ins.guardianes.request.NewsRequester;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;
import pe.gob.ins.guardianes.view.dialog.LoadDialog;

public class NewsActivity extends BaseAppCompatActivity implements NewsListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.news);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        new NewsRequester(this).getAll(new NewsHandler());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.news, menu);

        return true;
    }

    private class NewsHandler implements RequestListener<List<News>> {

        private final LoadDialog loadDialog = new LoadDialog();

        @Override
        public void onStart() {
            loadDialog.show(getFragmentManager(), LoadDialog.TAG);
        }

        @Override
        public void onError(final Exception e) {
            loadDialog.dismiss();
            new DialogBuilder(NewsActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.dcontent_news_empty)
                    .positiveText(R.string.ok)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            onBackPressed();
                        }
                    })
                    .show();
        }

        @Override
        public void onSuccess(final List<News> newsList) {
            loadDialog.dismiss();
            recyclerView.setAdapter(new NewsAdapter(NewsActivity.this, newsList));
        }
    }

    @Override
    public void onNewsSelect(final News news) {
        new DialogBuilder(NewsActivity.this).load()
                .title(R.string.attention)
                .content(R.string.open_link)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .onPositive(new MaterialDialog.SingleButtonCallback() {

                    @Override
                    public void onClick(@NonNull final MaterialDialog dialog, @NonNull final DialogAction which) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(news.getLink())));
                    }

                }).show();
    }
}

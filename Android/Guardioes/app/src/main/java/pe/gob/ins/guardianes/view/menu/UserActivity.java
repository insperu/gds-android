package pe.gob.ins.guardianes.view.menu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.AddressHelper;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DateFormat;
import pe.gob.ins.guardianes.helper.DialogBuilder;
import pe.gob.ins.guardianes.helper.Helper;
import pe.gob.ins.guardianes.helper.Mask;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.Country;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.request.UserRequester;
import pe.gob.ins.guardianes.request.base.AuthRequester;
import pe.gob.ins.guardianes.request.base.RequestHandler;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.CountryAdapter;
import pe.gob.ins.guardianes.view.HomeActivity;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;

public class UserActivity extends BaseAppCompatActivity {

    private static final String PERU = "PE";

    @BindView(R.id.text_view_message)
    TextView textViewMessage;

    @BindView(R.id.edit_lastname)
    AppCompatEditText editTextLastname;

    @BindView(R.id.edit_firstname)
    AppCompatEditText editTextFirstname;

    @BindView(R.id.edit_text_parent_other)
    AppCompatEditText editTextParent;

    @BindView(R.id.edit_text_race_other)
    AppCompatEditText editTextRace;

    @BindView(R.id.edit_text_birth_date)
    AppCompatEditText editTextBirthDate;

    @BindView(R.id.text_view_state)
    TextView textViewState;

    @BindView(R.id.spinner_gender)
    Spinner spinnerGender;

    @BindView(R.id.text_view_race)
    TextView textViewRace;

    @BindView(R.id.text_view_city)
    TextView textViewCity;

    @BindView(R.id.text_view_province)
    TextView textViewProvince;

    @BindView(R.id.spinner_race)
    Spinner spinnerRace;

    @BindView(R.id.spinner_parent)
    Spinner spinnerParent;

    @BindView(R.id.spinner_country)
    Spinner spinnerCountry;

    @BindView(R.id.spinner_province)
    Spinner spinnerProvince;

    @BindView(R.id.spinner_state)
    Spinner spinnerState;

    @BindView(R.id.spinner_city)
    Spinner spinnerCity;

    private User user;
    private boolean main;
    private boolean create;
    private CountryAdapter spinnerAdapter;
    private Country peru = null;

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.user);

        main = getIntent().getBooleanExtra(Constants.Bundle.MAIN_MEMBER, false);
        user = Parcels.unwrap(getIntent().getParcelableExtra(Constants.Bundle.USER));
        setCreate(user == null);

        loadView();
        for (Country obj : spinnerAdapter.getContries()) {
            if (obj.getName().equalsIgnoreCase("Perú"))
                peru = obj;
        }

        spinnerCountry.post(new Runnable() {
            @Override
            public void run() {
                spinnerCountry.setSelection(spinnerAdapter.getPosition(peru));
            }
        });

        if (create) {
            user = new User();

        } else {
            load(user);
        }
    }

    private void load(final User user) {
        Log.d("USER load", user.toString());
        editTextLastname.setText(user.getLastname());
        editTextFirstname.setText(user.getFirstname());

        spinnerGender.setSelection(user.getGender().equalsIgnoreCase("Masculino") ? 1 : 2);

        List<String> races = toList(getResources().getStringArray(R.array.race_array));
        for (int i = 0; i < races.size(); i++) {
            if (races.get(i).equals(user.getRace())) {
                spinnerRace.setSelection(i);
                break;
            }
        }

        if (spinnerRace.getSelectedItemId() == 0) {
            spinnerRace.setSelection(races.size() - 1);
            editTextRace.setText(user.getRace());
        }

        List<String> relationship = toList(getResources().getStringArray(R.array.relationship_array));
        Log.d("Selected spinner id:", String.valueOf(spinnerParent.getSelectedItemId()));
        Log.d("Selected spinner value:", String.valueOf(spinnerParent.getSelectedItem()));

        for (int i = 0; i < relationship.size(); i++) {
            if (relationship.get(i).equals(user.getRelationship())) {
                spinnerParent.setSelection(i);
                break;
            }

        }

        if (spinnerParent.getSelectedItemId() == 0) {
            spinnerParent.setSelection(relationship.size() - 1);
            editTextParent.setText(user.getRelationship());
        }

        final String format = DateFormat.getDate(user.getDob(), "dd/MM/yyyy");

        editTextBirthDate.setText(format);

        if (user.getCountry() != null) {

            for (int i = 0; i < spinnerAdapter.getCount(); i++) {

                Country country = spinnerAdapter.getItem(i);
                if (country != null && user.getCountry().equals(country.getName())) {
                    final int finalI = i;
                    spinnerCountry.post(new Runnable() {
                        @Override
                        public void run() {
                            spinnerCountry.setSelection(finalI);
                        }
                    });

                    Log.d("Country", ((Country) spinnerCountry.getSelectedItem()).getName());
                }

            }

            if (user.getCountry().equalsIgnoreCase(peru.getName())) {
                List<String> states = AddressHelper.getDepartments(toList(getResources().getStringArray(R.array.departments_provinces_districts)));
                for (int i = 0; i < states.size(); i++) {
                    if (states.get(i).equals(user.getState())) {
                        final int statePosition = i;
                        spinnerState.post(new Runnable() {
                            @Override
                            public void run() {
                                spinnerState.setSelection(statePosition);
                                List<String> provinces = AddressHelper.getProvinces(toList(getResources().getStringArray(R.array.departments_provinces_districts)), user.getState());
                                for (int j = 0; j < provinces.size(); j++) {
                                    if (provinces.get(j).equals(user.getProvince())) {
                                        final int provincePosition = j;
                                        spinnerProvince.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                spinnerProvince.setSelection(provincePosition);
                                                List<String> cities = AddressHelper.getDistricts(toList(getResources().getStringArray(R.array.departments_provinces_districts)), user.getProvince());
                                                for (int i = 0; i < cities.size(); i++) {
                                                    if (cities.get(i).equals(user.getCity())) {
                                                        final int finalI = i;
                                                        spinnerCity.post(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                spinnerCity.setSelection(finalI);
                                                            }
                                                        });
                                                        break;
                                                    }
                                                }
                                            }
                                        });
                                        break;
                                    }
                                }
                            }
                        });
                        break;
                    }
                }
            }
        }

//        spinnerProfile.setSelection(user.getProfile());
    }

    private List<String> toList(final String[] valueArray) {

        final List<String> valueList = new LinkedList<>(Arrays.asList(valueArray));

        valueList.add(0, getString(R.string.select));

        return valueList;
    }

    private void loadView() {

        editTextBirthDate.addTextChangedListener(Mask.insert("##/##/####", editTextBirthDate));

        spinnerGender.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.gender_array))));
        spinnerRace.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.race_array))));
        spinnerRace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                findViewById(R.id.ll_other_race).setVisibility(
                        (spinnerRace.getSelectedItem().equals("Otro")) ? View.VISIBLE : View.GONE
                );
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerParent.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.relationship_array))));

        spinnerParent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                findViewById(R.id.linear_layout_parent_other).setVisibility(
                        (spinnerParent.getSelectedItem().equals("Otros")) ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final List<Country> countryList = Helper.loadCountry();

        countryList.add(0, new Country(0, "", getString(R.string.select)));

        Collections.sort(countryList);
        spinnerAdapter = new CountryAdapter(this, CountryAdapter.Type.STANDARD, countryList);
        spinnerCountry.setAdapter(spinnerAdapter);

        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int position, final long id) {
                String codeCountrySelected = countryList.get(position).getCode();

                textViewState.setVisibility(codeCountrySelected.equals(PERU) ? View.VISIBLE : View.GONE);
                spinnerState.setVisibility(codeCountrySelected.equals(PERU) ? View.VISIBLE : View.GONE);
                textViewProvince.setVisibility(codeCountrySelected.equals(PERU) ? View.VISIBLE : View.GONE);
                spinnerProvince.setVisibility(codeCountrySelected.equals(PERU) ? View.VISIBLE : View.GONE);
                textViewCity.setVisibility(codeCountrySelected.equals(PERU) ? View.VISIBLE : View.GONE);
                spinnerCity.setVisibility(codeCountrySelected.equals(PERU) ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> adapterView) {

            }
        });

        pe.gob.ins.guardianes.view.ItemAdapter itemAdapter = new pe.gob.ins.guardianes.view.ItemAdapter(this, AddressHelper.getDepartments(
                toList(getResources().getStringArray(R.array.departments_provinces_districts))));
        spinnerState.setAdapter(itemAdapter);

        final UserActivity this_activity = this;

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String state = spinnerState.getSelectedItem().toString();
                final pe.gob.ins.guardianes.view.ItemAdapter province_adapter = new pe.gob.ins.guardianes.view.ItemAdapter(this_activity, AddressHelper.getProvinces(
                        toList(getResources().getStringArray(R.array.departments_provinces_districts)), state));
                spinnerProvince.setAdapter(province_adapter);
                spinnerProvince.setSelection(province_adapter.getPosition(user.getProvince()));
                spinnerProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String province = spinnerProvince.getSelectedItem().toString();
                        pe.gob.ins.guardianes.view.ItemAdapter adapter = new pe.gob.ins.guardianes.view.ItemAdapter(this_activity, AddressHelper.getDistricts(
                                toList(getResources().getStringArray(R.array.departments_provinces_districts)), province));
                        spinnerCity.setAdapter(adapter);
                        spinnerCity.setSelection(adapter.getPosition(user.getCity()));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (user != null && user.getState() != null) {
            spinnerState.setSelection(itemAdapter.getPosition(user.getState()));
        }
        if (main) {

            textViewMessage.setText(R.string.message_fields);

            findViewById(R.id.linear_layout_parent).setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.button_save)
    public void onAdd() {
        add();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();

        } else {

            super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void add() {
        user.setFirstname(editTextFirstname.getText().toString());
        user.setLastname(editTextLastname.getText().toString());
        user.setGender(spinnerGender.getSelectedItem().toString());
        user.setDob(editTextBirthDate.getText().toString().trim());

        final String country = ((Country) spinnerCountry.getSelectedItem()).getName();

        user.setCountry(country);

        if (country.equals(getString(R.string.peru))) {
            user.setState(spinnerState.getSelectedItem().toString());
            user.setProvince(spinnerProvince.getSelectedItem().toString());
            user.setCity(spinnerCity.getSelectedItem().toString());
        } else {
            user.setState("N/A");
            user.setProvince("N/A");
            user.setCity("N/A");
        }

        final String parent = spinnerParent.getSelectedItem().toString();
        user.setRelationship((parent.equalsIgnoreCase("Otros")) ? editTextParent.getText().toString() : parent);

        final String race = spinnerRace.getSelectedItem().toString();
        user.setRace((race.equalsIgnoreCase("Otro")) ? editTextRace.getText().toString() : race);

        if (validate(user)) {
            user.setDob(DateFormat.getDate(user.getDob()));
            Log.d("USER /user/update", user.toString());
            final String url = main ? "/user/update" : create ? "/household/create" : "/household/update";

            String id = "";
            if (!main) {
                User mainUser = new PrefManager(UserActivity.this).get(Constants.Pref.USER, User.class);
                if (mainUser != null) {
                    id = mainUser.getId();
                }
            }

            new UserRequester(this).addOrUpdate(url, user, main ? null : id, new RequestHandler<String>(this) {

                @Override
                public void onError(final Exception e) {
                    super.onError(e);
                    e.printStackTrace();

                    new DialogBuilder(UserActivity.this).load()
                            .title(R.string.attention)
                            .content(R.string.error_add_new_member)
                            .positiveText(R.string.ok)
                            .show();
                }

                @Override
                public void onSuccess(final String message) {
                    super.onSuccess(message);
                    if (main) {
                        loadMain();
                    } else {
                        new DialogBuilder(UserActivity.this).load()
                                .title(R.string.attention)
                                .content(create ? R.string.member_added : R.string.member_updated)
                                .positiveText(R.string.ok)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {

                                    @Override
                                    public void onClick(@NonNull final MaterialDialog dialog, @NonNull final DialogAction which) {
                                        navigateTo(HomeActivity.class);
                                    }

                                }).show();
                    }

                }
            });
        }
    }

    private void updateUser() {

        if (user != null) {

            new PrefManager(UserActivity.this).put(Constants.Pref.USER, user);
        }
    }

    private void loadMain() {

        new AuthRequester(this).loadAuth(new RequestListener<User>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onError(final Exception e) {

            }

            @Override
            public void onSuccess(final User user) {

                updateUser();

                new DialogBuilder(UserActivity.this).load()
                        .title(R.string.attention)
                        .content(create ? R.string.member_added : R.string.member_updated)
                        .positiveText(R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {

                            @Override
                            public void onClick(@NonNull final MaterialDialog dialog, @NonNull final DialogAction which) {
                                navigateTo(HomeActivity.class);
                            }

                        }).show();
            }
        });
    }

    private boolean validate(final User user) {
        Log.d("UserActivity", main ? "TRUE" : "FALSE");
        return isFirstNameValid(user.getFirstname())
                && isGenderValid(user.getGender())
                && (main || isRelationshipValid(user.getRelationship()))
                && isBirthDateValid(user.getDob())
                && isRaceValid(user.getRace())
                && isHomeLocationValid(user.getCountry(), user.getState(), user.getProvince(), user.getCity());
    }


    private boolean isFirstNameValid(String firstName) {

        boolean isFirstNameValid = true;

        if (firstName.isEmpty()) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_first_name_alert)
                    .positiveText(R.string.ok)
                    .show();
            isFirstNameValid = false;
        }

        return isFirstNameValid;
    }

    private boolean isLastNameValid(String lastName) {

        boolean isLastNameValid = true;

        if (lastName.isEmpty()) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_last_name_alert)
                    .positiveText(R.string.ok)
                    .show();
            isLastNameValid = false;
        }

        return isLastNameValid;
    }


    private boolean isGenderValid(String gender) {

        boolean isGenderValid = !gender.equals(getString(R.string.select));

        if (!isGenderValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_gender_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isGenderValid;
    }

    private boolean isBirthDateValid(String birthDate) {

        boolean isBirthDateValid = true;

        if (!DateFormat.isDate(birthDate)) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.invalid_birth_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isBirthDateValid = false;
        } else if (main && DateFormat.getDateDiff(birthDate) < 13) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.low_birth_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isBirthDateValid = false;
        }

        return isBirthDateValid;
    }

    private boolean isRelationshipValid(String relationship) {
        boolean isRelationshipValid = !relationship.isEmpty() && !relationship.equals(getString(R.string.select));

        if (!isRelationshipValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_relationship_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isRelationshipValid;
    }

    private boolean isHomeLocationValid(String country, String state, String province, String city) {
        boolean isHomeLocationValid = true;

        if (country.equals(getString(R.string.peru))) {
            isHomeLocationValid = isStateValid(state) && isProvinceValid(province) && isCityValid(city);
        }

        return isHomeLocationValid;
    }

    private boolean isStateValid(String state) {
        boolean isStateValid = !state.equals(getString(R.string.select));

        if (!isStateValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_state_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isStateValid;
    }

    private boolean isProvinceValid(String province) {
        boolean isProvinceValid = !province.equals(getString(R.string.select));

        if (!isProvinceValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_province_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isProvinceValid;
    }

    private boolean isCityValid(String city) {

        boolean isCityValid = !city.equals(getString(R.string.select));

        if (!isCityValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_city_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isCityValid;
    }

    private boolean isRaceValid(String race) {
        boolean isRaceValid = !race.equals(getString(R.string.select));

        if (!isRaceValid) {
            new DialogBuilder(UserActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_race_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isRaceValid;
    }

    private void setCreate(final boolean create) {
        this.create = create;
    }
}

package pe.gob.ins.guardianes.view.menu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.view.base.BaseFragment;

public class AboutFragment extends BaseFragment {

    @BindView(R.id.message_about_content_01)
    TextView textViewAbout;

    @BindView(R.id.txt_version_build)
    TextView textViewVersionBuild;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup viewGroup, final Bundle bundle) {
        return inflater.inflate(R.layout.about, viewGroup, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle bundle) {

        bind(view);

        User user = new PrefManager(getActivity()).get(Constants.Pref.USER, User.class);

        String version = getString(R.string.version_name);
        if (user != null) {
            version = user.getVersionBuild() != null ? version = user.getVersionBuild() : version;
        }

        textViewVersionBuild.setText(this.getString(R.string.versao) + " " + version);
    }

    @Override
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);

        getSupportActionBar().setTitle(R.string.about);
    }

    @OnClick(R.id.centeias)
    public void onClickCenteias() {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://fs.unb.br/unb-fs/menu-superior/centeias-centro-de-tecnologias-educacionais-interativas-em-saude"));
        startActivity(intent);
    }

    @OnClick(R.id.unb)
    public void onClickUnb() {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://unb.br/"));
        startActivity(intent);
    }

    @OnClick(R.id.ins_colombia)
    public void onClickINSColombia() {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://www.ins.gov.co/Paginas/Inicio.aspx"));
        startActivity(intent);
    }
}

package pe.gob.ins.guardianes.request;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.request.base.BaseRequester;
import pe.gob.ins.guardianes.request.base.Method;
import pe.gob.ins.guardianes.request.base.RequestException;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.request.base.Requester;
import pe.gob.ins.guardianes.request.base.RequesterConfig;

public class SurveyRequester extends BaseRequester {

    public SurveyRequester(Context context) {
        super(context);
    }

    public void saveSurveyGood(final User user, final LatLng latLng, final RequestListener<Boolean> listener) {

        final String url = RequesterConfig.URL + "/survey/create";

        User savedUser = new PrefManager(getContext()).get(Constants.Pref.USER, User.class);

        final Map<String, Object> bodyMap = new HashMap<>();

        bodyMap.put("user_id", savedUser.getId());
        if (!user.getId().equals(savedUser.getId())) {
            bodyMap.put("household_id", user.getId());
        }

        bodyMap.put("lat", latLng.latitude);
        bodyMap.put("lon", latLng.longitude);
        bodyMap.put("no_symptom", "Y");
        bodyMap.put("platform", user.getPlatform());
        bodyMap.put("client", user.getClient());
        bodyMap.put("app_token", user.getAppToken());

        new Requester(getContext()).request(Method.POST, url, getAuthHeaderMap(), bodyMap, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    try {

                        final JSONObject jsonObject = new JSONObject(response.getResult());

                        if (jsonObject.getBoolean("error")) {
                            listener.onError(new RequestException(jsonObject.getString("message")));

                        } else {

                            listener.onSuccess(true);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    public void getSymptom(final RequestListener<String> listener) {

        final String url = RequesterConfig.URL + "/symptoms";

        new Requester(getContext()).request(Method.GET, url, getAuthHeaderMap(), new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception e, final Response<String> response) {

                if (e == null) {

                    listener.onSuccess(response.getResult());

                } else {

                    listener.onError(e);
                }
            }
        });
    }

    public void sendSurvey(final Map<String, Object> bodyMap, final RequestListener<String> listener) {

        final String url = RequesterConfig.URL + "/survey/create";

        new Requester(getContext()).request(Method.POST, url, getAuthHeaderMap(), bodyMap, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception e, final Response<String> response) {

                if (e == null) {

                    listener.onSuccess(response.getResult());

                } else {

                    listener.onError(e);
                }
            }
        });
    }

    public void hasSurvey(final Calendar calendar, final RequestListener<Boolean> listener) {

        final String url = RequesterConfig.URL + "/user/calendar/day";

        final Map<String, String> paramMap = new HashMap<>();

        paramMap.put("day", String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        paramMap.put("month", String.valueOf(calendar.get(Calendar.MONTH) + 1));
        paramMap.put("year", String.valueOf(calendar.get(Calendar.YEAR)));

        new Requester(getContext()).request(Method.GET, url, paramMap, getAuthHeaderMap(), null, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    try {

                        final JSONObject jsonObject = new JSONObject(response.getResult());

                        if (jsonObject.getBoolean("error")) {

                            listener.onError(new RequestException());

                        } else {

                            if (jsonObject.getJSONArray("data").length() == 0) {

                                listener.onSuccess(false);

                            } else {

                                listener.onSuccess(true);
                            }
                        }

                    } catch (JSONException e) {

                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }

    public void hasSurveyToday(final RequestListener<Integer> listener) {

        final String url = RequesterConfig.URL + "/user/calendar/day";

        final Map<String, String> paramMap = new HashMap<>();

        final Calendar calendar = Calendar.getInstance();

        paramMap.put("day", String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        paramMap.put("month", String.valueOf(calendar.get(Calendar.MONTH)));
        paramMap.put("year", String.valueOf(calendar.get(Calendar.YEAR)));

        new Requester(getContext()).request(Method.GET, url, paramMap, getAuthHeaderMap(), null, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    try {

                        final JSONObject jsonObject = new JSONObject(response.getResult());
                        Log.d("jsonObject", "" + jsonObject.toString());
                        if (jsonObject.getBoolean("error")) {

                            listener.onError(new RequestException());

                        } else {

                            listener.onSuccess(jsonObject.getJSONArray("data").length());
                        }

                    } catch (JSONException e) {

                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }
}

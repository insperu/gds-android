package pe.gob.ins.guardianes.helper;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class DateFormat {

    public static String getDate(String date, String format) {

        SimpleDateFormat userFormat = new SimpleDateFormat(format);
        SimpleDateFormat apiFormat = new SimpleDateFormat("yyyy-MM-dd");
        String strReturn = "";

        try {
            Date userDate = apiFormat.parse(date);
            strReturn = userFormat.format(userDate);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            return strReturn;
        }
    }

    public static String getDate(String date) {

        SimpleDateFormat userFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat apiFormat = new SimpleDateFormat("yyyy-MM-dd");
        String strReturn = "";

        try {
            Date userDate = userFormat.parse(date);
            strReturn = apiFormat.format(userDate);
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            return strReturn;
        }
    }

    public static int getDateDiff(String date) {

        Date userDate;
        Date today;
        today = CalendarDay.today().getDate();
        int diffDate;
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            userDate = format.parse(date);
            long diff_in_millis = today.getTime() - userDate.getTime();

            if (diff_in_millis < 0) {
                diffDate = -1;
            } else {
                final double DENOMINATOR = 31536000000.0;
                Double diff_in_years = diff_in_millis / DENOMINATOR;
                diffDate = diff_in_years.intValue();
            }
        } catch (ParseException e) {
            try {
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                userDate = format2.parse(date);
                long diff_in_millis = today.getTime() - userDate.getTime();

                if (diff_in_millis < 0) {
                    diffDate = -1;
                } else {
                    final double DENOMINATOR = 31536000000.0;
                    Double diff_in_years = diff_in_millis / DENOMINATOR;
                    diffDate = diff_in_years.intValue();
                }
            } catch (ParseException e2) {
                e2.printStackTrace();
                diffDate = -1;
            }
        }

        return diffDate;
    }

    public static int getTwitterDiff(String date) {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());

        Date userDate;
        Date today;
        int diffDate = -1;

        try {
            userDate = format.parse(date);
            today = new Date();

            long diff = today.getTime() - userDate.getTime();
            long diffHours = diff / (60 * 60 * 1000) % 24;
            diffDate = ((int) diffHours);

            if (diffDate < 0) {
                diffDate = 24 - (diffDate * -1);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diffDate;
    }

    public static boolean isDate(String date) {
        boolean isDate = true;
        try {
            String[] split = date.split("/");
            int day = Integer.parseInt(split[0]);
            int month = Integer.parseInt(split[1]);
            int year = Integer.parseInt(split[2]);
            if (year < 1900) {
                isDate = false;
            } else {
                if (month == 2) {
                    if (day > 29) {
                        isDate = false;
                    }
                } else if (day > 31) {
                    isDate = false;
                }
            }
        } catch (Exception e) {
            isDate = false;
        }
        return isDate;
    }
}

package pe.gob.ins.guardianes.view.account;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.helper.AddressHelper;
import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DateFormat;
import pe.gob.ins.guardianes.helper.DialogBuilder;
import pe.gob.ins.guardianes.helper.Helper;
import pe.gob.ins.guardianes.helper.Mask;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.Country;
import pe.gob.ins.guardianes.model.Gender;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.push.HashReceiver;
import pe.gob.ins.guardianes.push.RegisterService;
import pe.gob.ins.guardianes.request.UserRequester;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.view.CountryAdapter;
import pe.gob.ins.guardianes.view.ItemAdapter;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;
import pe.gob.ins.guardianes.view.dialog.LoadDialog;
import pe.gob.ins.guardianes.view.survey.StateActivity;
import pe.gob.ins.guardianes.view.welcome.TermActivity;

public class CreateAccountActivity extends BaseAppCompatActivity {

    private static final int REQUEST_MAIL = 6669;

    private static final int NONE = 0;

    private static final String PERU = "PE";

    private static final String SOCIAL_FRAGMENT = "social_fragment";
    private final LoadDialog loadDialog = new LoadDialog();

    @BindView(R.id.linear_layout_social_account)
    LinearLayout llSocial;

    @BindView(R.id.linear_layout_account)
    ScrollView llAccount;

    @BindView(R.id.text_race_other)
    TextInputLayout textRaceOther;

    @BindView(R.id.edit_text_mail)
    EditText etEmail;

    @BindView(R.id.edit_text_password)
    EditText etPassword;

    @BindView(R.id.edit_text_name)
    EditText etFirstName;

    @BindView(R.id.edit_text_nickname)
    EditText etLastName;

    @BindView(R.id.edit_text_birth_date)
    EditText etBirthDate;

    @BindView(R.id.edit_text_race_other)
    EditText etRaceOther;

    @BindView(R.id.text_view_state)
    TextView tvDepartment;

    @BindView(R.id.text_view_province)
    TextView tvProvince;

    @BindView(R.id.text_view_city)
    TextView tvDistrict;

    @BindView(R.id.text_view_race)
    TextView tvRace;

    @BindView(R.id.spinner_race)
    Spinner spRace;

    @BindView(R.id.spinner_gender)
    Spinner spGender;

    @BindView(R.id.spinner_country)
    Spinner spCountry;

    @BindView(R.id.spinner_city)
    Spinner spDistrict;

    @BindView(R.id.spinner_province)
    Spinner spProvince;

    @BindView(R.id.spinner_state)
    Spinner spDepartment;

    private SocialFragment socialFragment;
    private State state = State.SOCIAL;
    private User user = new User();
    private CountryAdapter spinnerAdapter;
    private Country peru = null;
    private HashReceiver receiver = new HashReceiver() {

        public void onHash(final String hash) {

            user.setFirstname(etFirstName.getText().toString().trim());
            user.setLastname("");
            user.setDob(etBirthDate.getText().toString().trim().toLowerCase());

            final String gender = Gender.getBy(spGender.getSelectedItemPosition()).getValue();
            user.setGender(gender);

            String race = spRace.getSelectedItem().toString();
            if (race.equals("Otro")) {
                race = etRaceOther.getText().toString();
            }
            user.setRace(race);
            final String country = ((Country) spCountry.getSelectedItem()).getName();
            user.setCountry(country);
            if (!((Country) spCountry.getSelectedItem()).getCode().equals(PERU)) {
                user.setState(getString(R.string.na));
                user.setProvince(getString(R.string.na));
                user.setCity(getString(R.string.na));
            } else {
                user.setState(spDepartment.getSelectedItem().toString());
                user.setProvince(spProvince.getSelectedItem().toString());
                user.setCity(spDistrict.getSelectedItem().toString());
            }
//            user.setProfile(spinnerProfile.getSelectedItemPosition());
            user.setEmail(etEmail.getText().toString().trim().toLowerCase());

            if (user.getTw() != null || user.getFb() != null || user.getGl() != null) {
                user.setPassword(user.getEmail());

            } else {

                user.setPassword(etPassword.getText().toString().trim());
            }

            user.setGcmToken(hash);
            new UserRequester(CreateAccountActivity.this).createAccount(user, new RequestListener<User>() {

                @Override
                public void onStart() {

                }

                @Override
                public void onError(final Exception e) {
                    loadDialog.dismiss();
                    try {
                        if (e.getMessage().equalsIgnoreCase("User already exists")) {
                            new DialogBuilder(CreateAccountActivity.this).load()
                                    .title(R.string.attention)
                                    .content("User already exists")
                                    .positiveText(R.string.ok)
                                    .show();
                        } else {
                            Toast.makeText(CreateAccountActivity.this, R.string.erro_new_user, Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(CreateAccountActivity.this, R.string.erro_new_user, Toast.LENGTH_LONG).show();
                    }
                    e.printStackTrace();
                }

                @Override
                public void onSuccess(final User type) {
                    loadDialog.dismiss();

                    final Bundle bundle = new Bundle();

                    user.setUserToken(type.getUserToken());
                    user.setVersionBuild(type.getVersionBuild());
                    user.setType(type.getType());
                    user.setRelationship(type.getRelationship());
                    user.setPath(type.getPath());
                    user.setId(type.getId());
                    user.setDob(type.getDob());
                    new PrefManager(CreateAccountActivity.this).put(Constants.Pref.USER, user);

                    bundle.putBoolean(Constants.Bundle.MAIN_MEMBER, true);

                    navigateTo(StateActivity.class, Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK, bundle);
                }
            });
        }
    };

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.create_account);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        load();

        getSocialFragment().setListener(new AccountHandler());

        for (Country obj : spinnerAdapter.getContries()) {
            if (obj.getName().equalsIgnoreCase("Perú"))
                peru = obj;
        }

        spCountry.post(new Runnable() {
            @Override
            public void run() {
                spCountry.setSelection(spinnerAdapter.getPosition(peru));
            }
        });
    }

    private List<String> toList(final String[] valueArray) {

        final List<String> valueList = new LinkedList<>(Arrays.asList(valueArray));

        valueList.add(0, getString(R.string.select));

        return valueList;
    }

    private void load() {

        etBirthDate.addTextChangedListener(Mask.insert("##/##/####", etBirthDate));

        spGender.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.gender_array))));

        spRace.setAdapter(new ItemAdapter(this, toList(getResources().getStringArray(R.array.race_array))));

        spRace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                textRaceOther.setVisibility(spRace.getSelectedItem().equals("Otro") ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spDepartment.setAdapter(
                new ItemAdapter(this, AddressHelper.getDepartments(
                        toList(getResources().getStringArray(R.array.departments_provinces_districts)))));

        final CreateAccountActivity this_activity = this;

        spDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String department = spDepartment.getSelectedItem().toString();
                spProvince.setAdapter(new ItemAdapter(this_activity, AddressHelper.getProvinces(
                        toList(getResources().getStringArray(R.array.departments_provinces_districts)),
                        department)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String province = spProvince.getSelectedItem().toString();
                spDistrict.setAdapter(new ItemAdapter(this_activity, AddressHelper.getDistricts(
                        toList(getResources().getStringArray(R.array.departments_provinces_districts)),
                        province)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final List<Country> countryList = Helper.loadCountry();

        Collections.sort(countryList);

        spinnerAdapter = new CountryAdapter(this, CountryAdapter.Type.STANDARD, countryList);

        spCountry.setAdapter(spinnerAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int position, final long id) {
                String codeCountrySelected = countryList.get(position).getCode();
                if (codeCountrySelected.equals(PERU)) {
                    tvDepartment.setVisibility(View.VISIBLE);
                    spDepartment.setVisibility(View.VISIBLE);
                    tvProvince.setVisibility(View.VISIBLE);
                    spProvince.setVisibility(View.VISIBLE);
                    tvDistrict.setVisibility(View.VISIBLE);
                    spDistrict.setVisibility(View.VISIBLE);
                } else {
                    tvDepartment.setVisibility(View.GONE);
                    spDepartment.post(new Runnable() {
                        @Override
                        public void run() {
                            spDepartment.setSelection(NONE);
                        }
                    });
                    spDepartment.setVisibility(View.GONE);
                    tvProvince.setVisibility(View.GONE);
                    spProvince.post(new Runnable() {
                        @Override
                        public void run() {
                            spProvince.setSelection(NONE);
                        }
                    });
                    spProvince.setVisibility(View.GONE);
                    tvDistrict.setVisibility(View.GONE);
                    spDistrict.post(new Runnable() {
                        @Override
                        public void run() {
                            spDistrict.setSelection(NONE);
                        }
                    });
                    spDistrict.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(final AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.privacy, menu);

        return true;
    }

    @Override
    public void onResume() {


        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(HashReceiver.HASH_RECEIVER));

        getTracker().setScreenName("Create Account Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
        super.onResume();
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            if (state == State.SOCIAL) {
                return super.onOptionsItemSelected(item);

            } else {

                handlerState();
            }

        } else {

            return super.onOptionsItemSelected(item);
        }

        return true;
    }

    public void onPrivacy(final MenuItem item) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.privacy);
        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.image_button_close);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onBackPressed() {

        if (state == State.SOCIAL) {
            super.onBackPressed();

        } else {
            handlerState();
        }
    }

    private void previous() {
        state = State.getBy(state.getId() - 1);
    }

    private void next() {
        state = State.getBy(state.getId() + 1);
    }

    private void handlerState() {

        if (state == State.ACCOUNT) {
            onPreviousAnimation(llSocial, llAccount);
        }
    }

    @OnClick(R.id.button_mail)
    public void onMail() {

        getTracker().send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Create Account by Email Button")
                .build());

        navigateForResult(TermActivity.class, REQUEST_MAIL);
    }

    private void onPreviousAnimation(final View visibleView, final View invisibleView) {

        final Animation slideIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);

        slideIn.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(final Animation animation) {
                visibleView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                previous();

                invisibleView.setVisibility(View.INVISIBLE);

                invisibleView.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(final Animation animation) {

            }
        });

        final Animation slideOut = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);

        slideOut.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(final Animation animation) {

            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                visibleView.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(final Animation animation) {

            }
        });

        visibleView.startAnimation(slideIn);

        invisibleView.startAnimation(slideOut);
    }

    private SocialFragment getSocialFragment() {

        if (socialFragment == null) {
            socialFragment = (SocialFragment) getFragmentManager().findFragmentByTag(SOCIAL_FRAGMENT);
        }

        return socialFragment;
    }

    @OnClick(R.id.button_create_account)
    public void onCreateAccount() {

        if (state == State.ACCOUNT && areAllFieldsValid()) {
            loadDialog.show(getFragmentManager(), LoadDialog.TAG);
            startService(new Intent(CreateAccountActivity.this, RegisterService.class));
        }
    }

    public void setUser(final User user) {
        this.user = user;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {

        if (requestCode == REQUEST_MAIL) {

            if (resultCode == RESULT_OK) {

                next();

                llSocial.setVisibility(View.INVISIBLE);
                llAccount.setVisibility(View.VISIBLE);
            }

        } else {

            getSocialFragment().onActivityResult(requestCode, resultCode, intent);
        }
    }

    private enum State {

        SOCIAL(1), ACCOUNT(2);

        private final int id;

        State(final int id) {
            this.id = id;
        }

        public static State getBy(final int id) {

            for (final State state : State.values()) {

                if (state.getId() == id) {
                    return state;
                }
            }

            throw new IllegalArgumentException("The State has not found.");
        }

        public int getId() {
            return id;
        }
    }

    private class AccountHandler implements AccountListener {

        @Override
        public void onCancel() {

        }

        @Override
        public void onError() {

        }

        @Override
        public void onNotFound(final User user) {

            next();

            etLastName.setText(user.getLastname());
            etEmail.setText(user.getEmail());

            llSocial.setVisibility(View.INVISIBLE);
            llAccount.setVisibility(View.VISIBLE);

            findViewById(R.id.text_layout_password).setVisibility(View.INVISIBLE);

            setUser(user);
        }

        @Override
        public void onSuccess(final User user) {

            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.already_registered)
                    .positiveText(R.string.ok)
                    .show();
        }
    }

    private boolean areAllFieldsValid() {

        return isFirstNameValid()
                && isGenderValid()
                && isBirthDateValid()
                && isHomeLocationValid()
                && isRaceValid()
                && isEmailValid()
                && isPasswordValid();
    }


    private boolean isFirstNameValid() {
        String firstName = etFirstName.getText().toString();

        boolean isFirstNameValid = true;

        if (firstName.isEmpty()) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_first_name_alert)
                    .positiveText(R.string.ok)
                    .show();
            isFirstNameValid = false;
        }

        return isFirstNameValid;
    }

    private boolean isLastNameValid() {
        String lastName = etLastName.getText().toString();

        boolean isLastNameValid = true;

        if (lastName.isEmpty()) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_last_name_alert)
                    .positiveText(R.string.ok)
                    .show();
            isLastNameValid = false;
        }

        return isLastNameValid;
    }


    private boolean isGenderValid() {

        boolean isGenderValid = spGender.getSelectedItemPosition() != NONE;

        if (!isGenderValid) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_gender_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isGenderValid;
    }

    private boolean isBirthDateValid() {

        String birthDate = etBirthDate.getText().toString().trim().toLowerCase();

        boolean isBirthDateValid = true;

        if (!DateFormat.isDate(birthDate)) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.invalid_birth_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isBirthDateValid = false;
        } else if(DateFormat.getDateDiff(birthDate) < 13) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.low_birth_date_alert)
                    .positiveText(R.string.ok)
                    .show();
            isBirthDateValid = false;
        }

        return isBirthDateValid;
    }

    private boolean isHomeLocationValid() {
        boolean isHomeLocationValid = true;

        if (((Country) spCountry.getSelectedItem()).getCode().equals(PERU)) {
            isHomeLocationValid = isStateValid() && isCityValid();
        }

        return isHomeLocationValid;
    }

    private boolean isStateValid() {
        boolean isStateValid = spDepartment.getSelectedItemPosition() != NONE;

        if (!isStateValid) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_state_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isStateValid;
    }

    private boolean isCityValid() {

        boolean isCityValid = spDistrict.getSelectedItemPosition() != NONE;

        if (!isCityValid) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_city_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isCityValid;
    }

    private boolean isRaceValid() {
        boolean isRaceValid = spRace.getSelectedItemPosition() != NONE;

        if (!isRaceValid) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_race_alert)
                    .positiveText(R.string.ok)
                    .show();
        }

        return isRaceValid;
    }

    private boolean isEmailValid() {
        String email = etEmail.getText().toString().toLowerCase();

        boolean isEmailValid = true;

        if (email.isEmpty()) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.empty_email_alert)
                    .positiveText(R.string.ok)
                    .show();
            isEmailValid = false;
        } else {
            try {
                if (email.split("@")[1].split("\\.").length < 2) {
                    isEmailValid = false;
                }
            } catch (Exception e) {
                isEmailValid = false;
            }

            if (!isEmailValid) {
                new DialogBuilder(CreateAccountActivity.this).load()
                        .title(R.string.attention)
                        .content(R.string.invalid_email_alert)
                        .positiveText(R.string.ok)
                        .show();
            }
        }

        return isEmailValid;
    }

    private boolean isPasswordValid() {
        String password = etPassword.getText().toString();

        boolean isPasswordValid = true;

        if (password.length() < 6) {
            new DialogBuilder(CreateAccountActivity.this).load()
                    .title(R.string.attention)
                    .content(R.string.invalid_password_alert)
                    .positiveText(R.string.ok)
                    .show();
            isPasswordValid = false;
        }

        return isPasswordValid;
    }
}

package pe.gob.ins.guardianes.request;

import android.content.Context;
import android.util.Log;

import pe.gob.ins.guardianes.helper.Constants;
import pe.gob.ins.guardianes.helper.DateFormat;
import pe.gob.ins.guardianes.helper.Utility;
import pe.gob.ins.guardianes.manager.PrefManager;
import pe.gob.ins.guardianes.model.User;
import pe.gob.ins.guardianes.request.base.BaseRequester;
import pe.gob.ins.guardianes.request.base.RequestException;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.request.base.Requester;
import pe.gob.ins.guardianes.request.base.RequesterConfig;
import pe.gob.ins.guardianes.helper.Logger;
import pe.gob.ins.guardianes.request.base.Method;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class UserRequester extends BaseRequester {

    private static final String TAG = UserRequester.class.getSimpleName();

    public UserRequester(final Context context) {
        super(context);
    }

    public void login(final String mail, final String password, final String hash, final RequestListener<User> listener) {

        final String url = RequesterConfig.URL + "/user/login";

        final Map<String, Object> bodyMap = new HashMap<>();

        bodyMap.put("email", mail);
        bodyMap.put("password", password);
        bodyMap.put("gcm_token", hash);

        listener.onStart();

        new Requester(getContext()).request(Method.POST, url, getHeaderMap(), bodyMap, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    if (isSuccess(response)) {

                        try {

                            final JSONObject json = new JSONObject(response.getResult());

                            if (json.getBoolean("error")) {

                                listener.onError(new RequestException());

                            } else {

                                final User user = new User();

                                JSONObject jsonObjectUser = json.getJSONObject("user");

                                user.setFirstname(jsonObjectUser.getString("firstname"));
                                user.setLastname(jsonObjectUser.getString("lastname"));
                                user.setEmail(jsonObjectUser.getString("email"));
                                user.setGender(jsonObjectUser.getString("gender"));
                                user.setId(jsonObjectUser.getString("id"));
                                user.setRace(jsonObjectUser.getString("race"));
                                user.setDob(jsonObjectUser.getString("dob"));
                                user.setUserToken(json.getString("token"));
                                user.setImage(jsonObjectUser.getInt("picture"));

                                if (jsonObjectUser.has("level")) {
                                    user.setLevel(jsonObjectUser.getInt("level"));
                                }

                                if (jsonObjectUser.has("xp")) {
                                    user.setEnergy(jsonObjectUser.getInt("xp"));
                                }

                                if (jsonObjectUser.has("answers")) {

                                    final JSONArray array = jsonObjectUser.getJSONArray("answers");

                                    for (int i = 0; i < array.length(); i++) {

                                        user.getPieceMap().put(i, array.getInt(i) == 1);
                                    }
                                }

                                if (jsonObjectUser.has("country")) {
                                    user.setCountry(jsonObjectUser.getString("country"));
                                }

                                if (jsonObjectUser.has("state")) {
                                    user.setState(jsonObjectUser.getString("state"));
                                }

                                if (jsonObjectUser.has("city")) {
                                    user.setCity(jsonObjectUser.getString("city"));
                                }

//                                if (jsonObjectUser.has("role")) {
//                                    user.setProfile(jsonObjectUser.getInt("role"));
//                                }

                                try {

                                    user.setHashtags(Utility.toList(jsonObjectUser.getJSONArray("hashtags")));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                PrefManager prefManager = new PrefManager(getContext());

                                prefManager.put(Constants.Pref.GCM_TOKEN_VERSION, "2");

                                if (prefManager.put(Constants.Pref.USER, user)) {
                                    listener.onSuccess(user);
                                }
                            }

                        } catch (final JSONException e) {
                            Logger.logDebug(TAG, e.getMessage());

                            listener.onError(new RequestException());
                        }

                    } else {

                        listener.onError(new RequestException());
                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }

    public void createAccount(final User user, final RequestListener<User> listener) {

        final String url = RequesterConfig.URL + "/user/create";

        final Map<String, Object> bodyMap = new HashMap<>();

        bodyMap.put("firstname", user.getFirstname());
        bodyMap.put("lastname", user.getLastname());
        bodyMap.put("email", user.getEmail());
        bodyMap.put("password", user.getPassword());
        bodyMap.put("client", user.getClient());
        if (Locale.getDefault().toString().equalsIgnoreCase("ar")) {
            try {
                String[] d = user.getDob().split("/");
                String nd = d[2] + "-" + d[1] + "-" + d[0];
                bodyMap.put("dob", nd);
            } catch (Exception ex) {
                bodyMap.put("dob", user.getDob());
            }
        } else {
            bodyMap.put("dob", DateFormat.getDate(user.getDob()));
        }
        bodyMap.put("gender", user.getGender());
        bodyMap.put("app_token", user.getAppToken());
        bodyMap.put("race", user.getRace());
        bodyMap.put("country", user.getCountry());
        bodyMap.put("city", user.getCity());
        bodyMap.put("province", user.getProvince());
        bodyMap.put("state", user.getState());
        bodyMap.put("platform", user.getPlatform());
        bodyMap.put("picture", "0");

        if (user.getTw() != null) {
            bodyMap.put("tw", user.getTw());
        }

        if (user.getFb() != null) {
            bodyMap.put("fb", user.getFb());
        }

        if (user.getGl() != null) {
            bodyMap.put("gl", user.getGl());
        }

        bodyMap.put("gcm_token", user.getGcmToken());

        listener.onStart();


        new Requester(getContext()).request(Method.POST, url, getHeaderMap(), bodyMap, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    if (isSuccess(response)) {

                        try {

                            final JSONObject json = new JSONObject(response.getResult());
                            Log.d("User Response", json.toString());
                            if (json.getBoolean("error")) {

                                listener.onError(new RequestException(response.getResult()));

                            } else {

                                final User user = new User();

                                JSONObject jsonObjectUser = json.getJSONObject("user");

                                user.setLastname(jsonObjectUser.getString("firstname"));
                                user.setEmail(jsonObjectUser.getString("lastname"));
                                user.setGender(jsonObjectUser.getString("gender"));
                                user.setImage(jsonObjectUser.getInt("picture"));
                                user.setId(jsonObjectUser.getString("id"));
                                user.setRace(jsonObjectUser.getString("race"));
                                user.setDob(jsonObjectUser.getString("dob"));
                                user.setUserToken(jsonObjectUser.getString("token"));

                                if (jsonObjectUser.has("level")) {
                                    user.setLevel(jsonObjectUser.getInt("level"));
                                }

                                if (jsonObjectUser.has("xp")) {
                                    user.setEnergy(jsonObjectUser.getInt("xp"));
                                }

                                if (jsonObjectUser.has("answers")) {

                                    final JSONArray array = jsonObjectUser.getJSONArray("answers");

                                    for (int i = 0; i < array.length(); i++) {

                                        user.getPieceMap().put(i, array.getInt(i) == 1);
                                    }
                                }

                                if (jsonObjectUser.has("country")) {
                                    user.setCountry(jsonObjectUser.getString("country"));
                                }

                                if (jsonObjectUser.has("state")) {
                                    user.setState(jsonObjectUser.getString("state"));
                                }

                                if (jsonObjectUser.has("city")) {
                                    user.setCity(jsonObjectUser.getString("city"));
                                }

                                PrefManager prefManager = new PrefManager(getContext());

                                prefManager.put(Constants.Pref.GCM_TOKEN_VERSION, "2");

                                if (prefManager.put(Constants.Pref.USER, user)) {
                                    listener.onSuccess(user);
                                }
                            }

                        } catch (final JSONException e) {
                            Logger.logDebug(TAG, e.getMessage());

                            listener.onError(new RequestException(e));
                        }

                    } else {

                        listener.onError(new RequestException(response.getResult()));
                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }

    public void validateSocialAccount(final String path, final String token, final RequestListener<User> listener) {

        final String url = RequesterConfig.URL + "/" + path + token;

        listener.onStart();

        new Requester(getContext()).request(Method.GET, url, getHeaderMap(), new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    try {

                        final JSONObject jsonObject = new JSONObject(response.getResult());

                        if (!jsonObject.getBoolean("error")) {

                            final JSONArray jsonArray = jsonObject.getJSONArray("data");

                            if (jsonArray.length() > 0) {

                                final JSONObject json = jsonArray.getJSONObject(0);

                                final String email = json.getString("email");
                                final String password = json.getString("email");

                                final User user = new User();

                                user.setEmail(email);
                                user.setPassword(password);

                                listener.onSuccess(user);

                            } else {

                                listener.onSuccess(null);
                            }

                        } else {

                            listener.onError(new RequestException());
                        }

                    } catch (final JSONException e) {

                        listener.onError(e);
                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }

    public void getAllProfiles(final String id, final RequestListener<List<User>> listener) {

        final String url = RequesterConfig.URL + RequesterConfig.Api.USER + "/household/" + id;

        listener.onStart();

        new Requester(getContext()).request(Method.GET, url, getAuthHeaderMap(), new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                if (error == null) {

                    try {

                        final List<User> parentList = new ArrayList<>();

                        final JSONObject json = new JSONObject(response.getResult());

                        if (!json.getBoolean("error")) {

                            final JSONArray jsonArray = json.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                final JSONObject jsonObject = jsonArray.getJSONObject(i);

                                final User user = new User(
                                        jsonObject.getString("firstname"),
                                        "",
                                        jsonObject.getString("id"),
                                        jsonObject.getString("dob"),
                                        jsonObject.getString("race"),
                                        jsonObject.getString("gender"));

                                try {

                                    user.setImage(jsonObject.getInt("picture"));

                                } catch (Exception e) {
                                    Logger.logError(TAG, e.getMessage());
                                }

                                try {

                                    user.setRelationship(jsonObject.getString("relationship"));

                                } catch (Exception e) {
                                    user.setRelationship("");
                                }

                                try {

                                    user.setEmail(jsonObject.getString("email"));

                                } catch (Exception e) {
                                }

                                if (jsonObject.has("country")) {
                                    user.setCountry(jsonObject.getString("country"));
                                }

                                if (jsonObject.has("state")) {
                                    user.setState(jsonObject.getString("state"));
                                }

                                if (jsonObject.has("province")) {
                                    user.setProvince(jsonObject.getString("province"));
                                }

                                if (jsonObject.has("city")) {
                                    user.setCity(jsonObject.getString("city"));
                                }
//                                if (jsonObject.has("role")) {
//                                    user.setProfile(jsonObject.getInt("role"));
//                                }

                                parentList.add(user);
                            }
                        }

                        listener.onSuccess(parentList);

                    } catch (final Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }

    public void getAllHousehold(final String id, final RequestListener<List<User>> listener) {

        final String url = RequesterConfig.URL + RequesterConfig.Api.USER + "/household/" + id;

        listener.onStart();

        new Requester(getContext()).request(Method.GET, url, getAuthHeaderMap(), new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                final List<User> parentList = new ArrayList<>();
                if (error == null) {

                    if (response.getHeaders().code() == 200) {

                        try {

                            final JSONObject json = new JSONObject(response.getResult());

                            if (!json.getBoolean("error")) {

                                final JSONArray jsonArray = json.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    final JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    final User user = new User(
                                            jsonObject.getString("firstname"),
                                            "",
                                            jsonObject.getString("id"),
                                            jsonObject.getString("dob"),
                                            jsonObject.getString("race"),
                                            jsonObject.getString("gender"));

                                    try {

                                        user.setImage(jsonObject.getInt("picture"));

                                    } catch (Exception e) {

                                    }

                                    if (jsonObject.has("country")) {
                                        user.setCountry(jsonObject.getString("country"));
                                    }

                                    if (jsonObject.has("state")) {
                                        user.setState(jsonObject.getString("state"));
                                    }

                                    if (jsonObject.has("city")) {
                                        user.setCity(jsonObject.getString("city"));
                                    }

//                                if (jsonObject.has("role")) {
//                                    user.setProfile(jsonObject.getInt("role"));
//                                }

                                    parentList.add(user);
                                }
                            }

                            listener.onSuccess(parentList);

                        } catch (final Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        listener.onSuccess(parentList);
                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }

    public void addOrUpdate(final String url, final User user, final String mainId, final RequestListener<String> listener) {

        final Map<String, Object> bodyMap = new HashMap<>();
        bodyMap.put("firstname", user.getFirstname());
        bodyMap.put("email", user.getEmail());
        bodyMap.put("password", user.getPassword());
        bodyMap.put("client", user.getClient());
        bodyMap.put("dob", user.getDob());
        bodyMap.put("gender", user.getGender());
        bodyMap.put("app_token", user.getAppToken());
        bodyMap.put("race", user.getRace());
        bodyMap.put("country", user.getCountry());
        bodyMap.put("city", user.getCity());
        bodyMap.put("province", user.getProvince());
        bodyMap.put("state", user.getState());
//        bodyMap.put("role", user.getProfile());
        bodyMap.put("platform", user.getPlatform());
        bodyMap.put("picture", user.getImage());
//        bodyMap.put("profile", user.getProfile());
        bodyMap.put("relationship", user.getRelationship());

        if (user.getGcmTokens().size() > 0) {
            String[] gcmTokensArr = new String[user.getGcmTokens().size()];
            user.getGcmTokens().toArray(gcmTokensArr);
            bodyMap.put("gcmTokens", gcmTokensArr);
        }
        bodyMap.put("id", user.getId());

        if (mainId != null) {
            bodyMap.put("user", mainId);
        }
        System.out.println(bodyMap.toString());

        listener.onStart();

        new Requester(getContext()).request(Method.POST, RequesterConfig.URL + url, getAuthHeaderMap(), bodyMap, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, Response<String> response) {

                if (error == null) {

                    if (isSuccess(response)) {

                        try {

                            final JSONObject json = new JSONObject(response.getResult());

                            if (json.getBoolean("error")) {

                                listener.onError(new RequestException());

                            } else {

                                final String message = json.getString("message");

                                listener.onSuccess(message);
                            }

                        } catch (final JSONException e) {
                            Logger.logDebug(TAG, e.getMessage());

                            listener.onError(new RequestException());
                        }

                    } else {

                        listener.onError(new RequestException());
                    }

                } else {

                    listener.onError(error);
                }
            }
        });

    }

    public void changePassword(User user, String password, String newPassword, final RequestListener<String> listener) {

        final Map<String, String> bodyMap = new HashMap<>();
        bodyMap.put("userId", user.getId());
        bodyMap.put("passwd", user.getId());
        bodyMap.put("passwdn", user.getId());
        new Requester(getContext()).request(Method.POST, RequesterConfig.URL + "/user/changepass", getAuthHeaderMap(), bodyMap, new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception error, Response<String> result) {
                if (error == null) {
                    listener.onSuccess("");
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    public void deleteAccount(final RequestListener<String> listener) {
        new Requester(getContext()).request(Method.DELETE, RequesterConfig.URL + "/user/delete/", getAuthHeaderMap(), new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception error, Response<String> response) {
                if (error == null && isSuccess(response)) {
                    listener.onSuccess(response.getResult());
                } else {
                    listener.onError(error);
                }
            }
        });
    }
}

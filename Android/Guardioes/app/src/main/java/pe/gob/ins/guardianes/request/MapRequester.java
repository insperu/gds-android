package pe.gob.ins.guardianes.request;

import android.content.Context;

import pe.gob.ins.guardianes.manager.Application;
import pe.gob.ins.guardianes.helper.Logger;
import pe.gob.ins.guardianes.model.Point;
import pe.gob.ins.guardianes.request.base.BaseRequester;
import pe.gob.ins.guardianes.request.base.Method;
import pe.gob.ins.guardianes.request.base.RequestListener;
import pe.gob.ins.guardianes.request.base.Requester;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Igor Morais
 */
public class MapRequester extends BaseRequester {

    private static final String TAG = NewsRequester.class.getSimpleName();

    public MapRequester(final Context context) {
        super(context);
    }

    // TODO: Removing self certificate, but on finish we add it. This is not the best way..
    public void loadPharmacy(final double latitude, final double longitude, final RequestListener<List<Point>> listener) {

        Ion.getDefault(getContext()).getHttpClient().getSSLSocketMiddleware().setSSLContext(null);
        Ion.getDefault(getContext()).getHttpClient().getSSLSocketMiddleware().setTrustManagers(null);

        final String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=pharmacy&location=" + latitude + "," + longitude + "&language=" + Locale.getDefault() + "&radius=10000&key=AIzaSyBLlphfLqqKDh_j9PwQjU_616JC3oW0drE";

        listener.onStart();

        new Requester(getContext()).request(Method.GET, url, new FutureCallback<Response<String>>() {

            @Override
            public void onCompleted(final Exception error, final Response<String> response) {

                // TODO: Removing self certificate, but on finish we add it. This is not the best way..
                if (getContext().getApplicationContext() instanceof Application) {
                    ((Application) getContext().getApplicationContext()).loadAuth();
                }

                if (error == null) {

                    try {

                        final List<Point> pointList = new ArrayList<>();

                        JSONObject jsonObject = new JSONObject(response.getResult());

                        if (jsonObject.getString("status").equalsIgnoreCase("ok")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("results");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                jsonObject = jsonArray.getJSONObject(i);

                                JSONObject jsonObjectLocation = jsonObject.getJSONObject("geometry").getJSONObject("location");

                                final Point point = new Point();

                                point.setLatitude(jsonObjectLocation.getDouble("lat"));
                                point.setLongitude(jsonObjectLocation.getDouble("lng"));
                                point.setLogradouro(jsonObject.getString("formatted_address"));
                                point.setName(jsonObject.getString("name"));

                                pointList.add(point);
                            }

                            listener.onSuccess(pointList);
                        }

                    } catch (final Exception e) {
                        Logger.logError(TAG, e.getMessage());

                        listener.onError(e);
                    }

                } else {

                    listener.onError(error);
                }
            }
        });
    }
}

package pe.gob.ins.guardianes.view.tip;

import android.os.Bundle;

import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.view.base.BaseAppCompatActivity;
import com.google.android.gms.analytics.HitBuilders;

/**
 * Created by vinicius on 17/08/17.
 */

public class TravelerHealthActivity extends BaseAppCompatActivity {

    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.traveler_health);
    }

    @Override
    public void onResume() {
        super.onResume();

        getTracker().setScreenName("Traveler Health Screen - " + this.getClass().getSimpleName());
        getTracker().send(new HitBuilders.ScreenViewBuilder().build());
    }
}

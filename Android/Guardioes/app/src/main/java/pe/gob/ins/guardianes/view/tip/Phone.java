package pe.gob.ins.guardianes.view.tip;

import pe.gob.ins.guardianes.R;
import pe.gob.ins.guardianes.view.IMenu;

public enum Phone implements IMenu {

    FAMILY_AND_SEXUAL_VIOLENCE(1, R.string.family_and_sexual_violence, "100"),
    POLICEMAN(2, R.string.police_center, "105"),
    SAMU(3, R.string.samu, "106"),
    INFO_HEALTH(4, R.string.info_health, "013156660"),
    CIVIL_DEFENSE(5, R.string.civil_defense, "110"),
    HEALTH(6, R.string.health_phone, "113"),
    RED_CROSS(7, R.string.red_cross, "115"),
    FIREMAN(8, R.string.fireman, "116");

    private final int id;
    private final int name;
    private final String number;

    Phone(final int id, final int name, final String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    @Override
    public final int getId() {
        return id;
    }

    @Override
    public final int getName() {
        return name;
    }

    @Override
    public int getIcon() {
        return 0;
    }

    @Override
    public String getTag() {
        return null;
    }

    @Override
    public boolean isDialogFragment() {
        return false;
    }

    @Override
    public boolean isFragment() {
        return false;
    }

    @Override
    public boolean isActivity() {
        return false;
    }

    @Override
    public Class<?> getType() {
        return null;
    }


    public final String getNumber() {
        return number;
    }

    public static Phone getBy(final long id) {

        for (final Phone phone : Phone.values()) {

            if (phone.getId() == id) {
                return phone;
            }
        }

        throw new IllegalArgumentException("The Phone has not found.");
    }
}
